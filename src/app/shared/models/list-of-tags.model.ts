import {ListOfTagsInterfaces} from './interfaces/list-of-tags.interfaces';
import {UserInterface} from './interfaces/user.interface';
import {DeserializableInterface} from './interfaces/deserializable.interface';
import {SerializableInterface} from './interfaces/serializable.interface';
import {TagModel} from '../../modules/admin-panel/shared/models/tag.model';

export class ListOfTagsModel implements ListOfTagsInterfaces, DeserializableInterface<ListOfTagsInterfaces>, SerializableInterface<ListOfTagsInterfaces> {

  name: string;
  status: string;
  tags: TagModel[];

  deserialize(input: ListOfTagsInterfaces): this {

    return Object.assign(this, input);

  }

  serialize(): ListOfTagsInterfaces {

    return Object.assign({} as ListOfTagsInterfaces, this);

  }

}
