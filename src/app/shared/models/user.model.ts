import {UserInterface} from './interfaces/user.interface';
import {DeserializableInterface} from './interfaces/deserializable.interface';
import {SerializableInterface} from './interfaces/serializable.interface';
import {isNullOrUndefined} from 'util';
import {DeviceUUID} from '../../../../node_modules/device-uuid/lib/device-uuid';
import {environment} from '../../../environments/environment';

export class UserModel implements UserInterface, DeserializableInterface<UserInterface>, SerializableInterface<UserInterface> {

  constructor() {

    this.deviceUUID = new DeviceUUID().get();

  }

  id: number;
  email: string;
  firstName: string;
  lastName: string;
  status: number;

  roles: string[];
  accessToken: string;
  expiresIn: string;
  refreshToken: string;

  deviceUUID: string;

  /**
   * return Object
   */
  static loadFromLocalStorage(): UserModel | null {

    const user = JSON.parse(localStorage.getItem('salo-sale-admin-panel-user-' + (environment.production ? 'prod' : 'dev')));

    if (isNullOrUndefined(user)) {

      return null;

    } else {

      return new UserModel().deserialize(user);

    }

  }

  deserialize(input: UserInterface): this {

    return Object.assign(this, input);

  }

  serialize(): UserInterface {

    return Object.assign({} as UserInterface, this);

  }

  /**
   * return boolean
   */
  saveToLocalStorage(): boolean {

    localStorage.setItem('salo-sale-admin-panel-user-' + (environment.production ? 'prod' : 'dev'), JSON.stringify(this.serialize()));
    return true;

  }

  accessTokenIsActive(): boolean {

    const date = this.expiresIn.split(' ')[0];

    if (!isNullOrUndefined(this.expiresIn)) {

      return (new Date(date).getTime()) >= Date.now();

    }

    return false;

  }

}
