import {RequestConfigurationInterface} from './interfaces/request-configuration.interface';
import {RequestParamsInterface} from './interfaces/request-params.interface';
import {DeserializableInterface} from './interfaces/deserializable.interface';
import {SerializableInterface} from './interfaces/serializable.interface';

export class RequestConfigurationModel implements
  RequestConfigurationInterface,
  DeserializableInterface<RequestConfigurationInterface>,
  SerializableInterface<RequestConfigurationInterface> {

  params?: RequestParamsInterface;
  path?: 'auth' | 'admin' | 'client';
  subUrl?: string;
  url: string;
  useUrlData?: boolean;

  constructor() {

    this.subUrl = '';
    this.params = null;
    this.useUrlData = true;
    this.path = 'client';

  }

  deserialize(input: RequestConfigurationInterface): this {

    return Object.assign(this, input);

  }

  serialize(): RequestConfigurationInterface {

    return Object.assign({} as RequestConfigurationInterface, this);

  }

}
