export interface RequestParamsInterface {

  useContentType?: boolean;
  type?: string;
  body?: any;
  options?: any;

}
