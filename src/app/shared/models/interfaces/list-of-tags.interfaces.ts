import {TagInterface} from '../../../modules/admin-panel/shared/models/interfaces/tag.interface';

export interface ListOfTagsInterfaces {

  name: string;
  status: string;
  tags: TagInterface[];

}
