import {RequestParamsInterface} from './request-params.interface';

export interface RequestConfigurationInterface {

  url: string;
  params?: RequestParamsInterface;
  subUrl?: string;
  useUrlData?: boolean;
  path?: 'auth' | 'admin' | 'client';

}
