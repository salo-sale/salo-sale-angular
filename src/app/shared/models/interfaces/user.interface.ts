export interface UserInterface {

  id: number;
  firstName: string;
  lastName: string;
  email: string;
  roles: string[];
  status: number;

  accessToken: string;
  expiresIn: string;
  refreshToken: string;

}
