import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {ApiService} from './api.service';
import {UserModel} from '../models/user.model';
import {ResetPasswordModel} from '../../modules/auth-panel/shared/models/reset-password.model';
import {isBoolean, isNullOrUndefined} from 'util';
import {RequestConfigurationModel} from '../models/request-configuration.model';
import {UserInterface} from '../models/interfaces/user.interface';
import {RegistrationModel} from '../../modules/auth-panel/shared/models/registration.model';
import {NotificationService} from '../../modules/admin-panel/shared/modules/notification/shared/services/notification.service';
import {NotificationStyle} from '../../modules/admin-panel/shared/modules/notification/shared/enums/notification-style.enum';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  user: UserModel;

  private initialized: boolean;

  constructor(
    private router: Router,
    public apiService: ApiService
  ) {

    this.initialized = false;

  }

  init() {

    if (!this.initialized) {

      this.initialized = true;

      this.user = UserModel.loadFromLocalStorage();

      this.apiService.init(isNullOrUndefined(this.user) ? new UserModel() : this.user);

      if (this.isLoggedIn) {

        if (this.user.accessTokenIsActive()) {

          this.checkAuthorization().then((result) => {

            // console.log(result);

          });

        } else {

          this.changeAccessToken().then((result) => {

            // console.log(result);

          });

        }

      }

    }

  }

  restInit() {

    if (this.initialized) {

      this.initialized = false;
      this.init();

    }

  }

  private changeAccessToken(): Promise<boolean> {

    return new Promise<boolean>(async resolve => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'auth',
        params: {
          body: {
            'refreshToken': this.user.refreshToken,
          },
          type: 'put',
        },
        url: 'refresh-token'
      });

      console.warn('Access token is not expires');

      await this.apiService.request(requestConfiguration).subscribe(async (data: UserInterface) => {

        // TODO
        console.log(data);
        this.user = new UserModel().deserialize(data);
        await this.user.saveToLocalStorage();
        await this.apiService.restInit();
        await this.apiService.init(this.user);
        await this.checkAuthorization();

        // console.log(data);

        resolve(true);

      }, error => {

        console.error(error);

        this.user = null;
        localStorage.setItem('salo-sale-admin-panel-user-' + (environment.production ? 'prod' : 'dev'), JSON.stringify(null));

        this.redirect('/');

        resolve(false);

      });

    });

  }

  public async checkAuthorization(): Promise<boolean> {

    return new Promise<boolean>(async resolve => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        useUrlData: false,
        url: 'user'
      });

      await this.apiService.request(requestConfiguration).subscribe((data: UserInterface) => {

        console.log(data);
        this.user = this.user.deserialize(data);
        this.apiService.restInit();
        this.apiService.init(this.user);
        this.user.saveToLocalStorage();

        resolve(true);

      }, error => {

        console.error('Token finish!');

        this.user = null;
        localStorage.setItem('salo-sale-admin-panel-user-' + (environment.production ? 'prod' : 'dev'), JSON.stringify(null));

        this.redirect('/');

        resolve(false);

      });

    });

  }

  public async logout() {

    const requestConfiguration = new RequestConfigurationModel().deserialize({
      url: 'logout',
      params: {
        type: 'delete'
      },
      path: 'auth'
    });

    await this.apiService.request(requestConfiguration, true).subscribe((data) => {

      console.log(data);

      localStorage.setItem('salo-sale-admin-panel-user-' + (environment.production ? 'prod' : 'dev'), JSON.stringify(null));

      setTimeout(() => {

        this.user = null;
        this.redirect('/');

      }, 500);

    });

  }

  public async resetPassword(email: string) {

    return new Promise<boolean>(async (resolve) => {

      // await this.apiService.request('reset-password', {
      //   type: 'post',
      //   body: {
      //     email: email
      //   }
      // }).subscribe((data) => {
      //
      //   resolve(true);
      //
      // }, error => {
      //
      //   console.error(error);
      //   this.errorSubject.next(error.error.error);
      //   resolve(false);
      //
      // });

    });

  }

  public async resetPasswordConfirm(resetPasswordModel: ResetPasswordModel) {

    return new Promise<boolean>(async (resolve) => {

      // await this.apiService.request('reset-password', {
      //   type: 'put',
      //   body: resetPasswordModel.serialize()
      // }).subscribe((data) => {
      //
      //   resolve(true);
      //
      // });

    });

  }

  public async registration(registrationModel: RegistrationModel): Promise<boolean> {

    return new Promise<boolean>(async resolve => {

      this.user = new UserModel();
      this.apiService.restInit();
      this.apiService.init(this.user);

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        params: {
          body: {
            'email': registrationModel.email,
            'password': registrationModel.password,
            'agreements': registrationModel.agreementsIds,
            'localityId': 3,
            'platform': 'DESKTOP', // TODO
            'fcmToken': 'ADMIN_PANEL' // TODO
          },
          type: 'post',
        },
        path: 'auth',
        url: 'registration'
      });

      await this.apiService.request(requestConfiguration).subscribe(async (data: {
        loginData: UserInterface,
        showCompensationBanner: boolean
      }) => {

        console.log(data);

        this.user = new UserModel().deserialize(data.loginData);
        await this.user.saveToLocalStorage();
        await this.apiService.restInit();
        await this.apiService.init(this.user);

        resolve(await this.checkAuthorization());

      }, error => {

        console.log(error);

        resolve(false);

      });

    });

  }

  public async login(email: string, password: string): Promise<boolean> {

    return new Promise<boolean>((async resolve => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        params: {
          body: {
            'email': email,
            'password': password,
            'platform': 'DESKTOP', // TODO
            'localityId': 3, // TODO
            'fcmToken': 'ADMIN_PANEL' // TODO
          },
          type: 'post'
        },
        path: 'auth',
        url: 'login',
      });

      this.user = new UserModel();
      this.apiService.restInit();
      this.apiService.init(this.user);

      await this.apiService.request(requestConfiguration).subscribe(async (data: UserInterface) => {

        console.log(data);

        this.user = new UserModel().deserialize(data);
        await this.user.saveToLocalStorage();
        await this.apiService.restInit();
        await this.apiService.init(this.user);

        resolve(await this.checkAuthorization());

      }, error => {

        resolve(false);

      });

    }));

  }

  get isLoggedIn(): boolean {

    return !isNullOrUndefined(this.user);

  }

  get emailIsConfirmed(): boolean {

    return isNullOrUndefined(this.user) ? false : this.user.status === 2;

  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    console.log(this.isLoggedIn);

    if (this.isLoggedIn && this.checkRoleAccess(['SUPER_ADMIN', 'ADMIN'])) {

      console.log('true is logg');

      console.log(route.routeConfig.path);

      if (route.routeConfig.path.length > 0) {

        if (route.routeConfig.path === 'login' || route.routeConfig.path === 'registration' || route.routeConfig.path === 'reset-password') {

          this.redirect('/admin');

        } else {

          return true;

        }

      } else {

        this.redirect('/admin');

      }

    } else if (route.routeConfig.path !== 'login' && route.routeConfig.path !== 'registration' && route.routeConfig.path !== 'reset-password') {

      console.log(route.routeConfig.path);
      // this.redirect('/login');

    }

    return true;

  }

  // canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot
  // ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //
  //   console.log([
  //     this.isLoggedIn,
  //     this.emailIsConfirmed,
  //     route.routeConfig.path === 'auth',
  //     route.routeConfig.path === 'admin',
  //     route.routeConfig.path.length === 0,
  //     this.checkRoleAccess(['SUPER_ADMIN', 'ADMIN'])
  //   ]);
  //
  //   if (this.isLoggedIn) {
  //
  //     if (this.emailIsConfirmed) {
  //
  //       if (
  //         route.routeConfig.path === 'auth'
  //       ) {
  //
  //         this.redirect('/admin/');
  //
  //       } else {
  //
  //         if (route.routeConfig.path === 'admin') {
  //
  //           return this.checkRoleAccess(['SUPER_ADMIN', 'ADMIN']);
  //
  //         }
  //
  //         return true;
  //
  //       }
  //
  //     } else {
  //
  //       if (
  //         route.routeConfig.path === 'auth'
  //       ) {
  //
  //         // console.log(route.firstChild.children[0].url[0].path);
  //
  //         if (route.firstChild.children[0].url[0].path === 'confirm-email') {
  //
  //           return true;
  //
  //
  //         } else {
  //
  //           this.redirect('/');
  //
  //         }
  //
  //       } else {
  //
  //         if (route.routeConfig.path.length === 0) {
  //
  //           this.redirect('/admin');
  //
  //         }
  //
  //         if (route.routeConfig.path === 'admin') {
  //
  //           return this.checkRoleAccess(['SUPER_ADMIN', 'ADMIN']);
  //
  //         }
  //
  //         return true;
  //
  //       }
  //
  //     }
  //
  //   }
  //
  //   // if (!this.isLoggedIn) {
  //   //
  //   //   if (route.firstChild.children[0].url[0].path === 'confirm-email') {
  //   //
  //   //     this.redirect('/login');
  //   //
  //   //   }
  //   //
  //   // }
  //
  //   return true;
  //
  // }

  public redirect(path: string) {

    this.router.navigateByUrl(path).then((responseNavigateByUrl) => {

      console.info('Redirect to ' + path + ', responseNavigateByUrl:', responseNavigateByUrl);

    });

  }

  checkRoleAccess(rolesHaveAccess: string[]) {

    if (this.isLoggedIn) {

      for (const role of rolesHaveAccess) {

        // console.log(role);

        if (!isNullOrUndefined(this.user.roles)) {

          if (this.user.roles.indexOf(role) > -1) {

            return true;

          }

        }

      }

    }

    return false;

  }

  public async sendConfirmEmail(verificationToken: string): Promise<boolean> {

    return new Promise<boolean>(async (resolve) => {

      const requestConfiguration: RequestConfigurationModel = new RequestConfigurationModel().deserialize({
        params: {
          type: 'post',
          body: {
            verificationToken: verificationToken
          }
        },
        path: 'auth',
        url: 'confirm-email'
      });

      await this.apiService.request(requestConfiguration).subscribe((data) => {

        console.log(data);
        if (isBoolean(data)) {

          resolve(data as boolean);

        } else {

          resolve(false);

        }

      });

    });

  }

}
