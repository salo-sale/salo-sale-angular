import {Component, EventEmitter, forwardRef, Inject, Input, OnInit, Output} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {AdvertisingBannerService} from '../../../shared/services/advertising-banner.service';
import {isNullOrUndefined, isNumber} from 'util';
import {AppService} from '../../../../../../../shared/services/app.service';
import {CityModel} from '../../../../../../../shared/models/city.model';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OfferModel} from '../../../../admin-panel-offer/shared/models/offer.model';
import {RecommendedOfferModel} from '../../../shared/models/recommended-offer.model';
import {environment} from '../../../../../../../../environments/environment';
import {OfferService} from '../../../../admin-panel-offer/shared/services/offer.service';

@Component({
  selector: 'app-admin-panel-advertising-banner-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  findByNameOrAddress: string;
  public dataAfterFind: Object;
  public model: RecommendedOfferModel;
  public submitted: boolean;

  constructor(
    private modalService: NgbModal,
    public router: Router,
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public advertisingBannerService: AdvertisingBannerService,
    private activatedRoute: ActivatedRoute,
    public appService: AppService,
    public offerService: OfferService
  ) {

    this.submitted = false;
    this.dataAfterFind = null;

    if (this.model === undefined) {

      this.model = this.advertisingBannerService.document;

      if (isNullOrUndefined(this.model)) {

        this.model = new RecommendedOfferModel();

      }

    } else {

      this.model.initForm();

    }

  }

  get cities(): CityModel[] {

    return this.appService.cities;

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      // console.log(params);
      // console.log(!isNullOrUndefined(params.offerId));


      if (!isNullOrUndefined(params.id)) {

        this.advertisingBannerService.getDocument(Number(params.id)).then((result) => {

          this.model = result;
          this.model.initForm();

        });

      }

    });

  }


  save() {

    this.submitted = true;

    if (this.model.validForm) {

      this.advertisingBannerService.save(this.model).then((result) => {

        if (isNumber(result) && result > 0) {

          this.model = new RecommendedOfferModel();

          this.router.navigateByUrl('admin/advertising-banner/view/' + result);

        }

      });

    }
  }

}
