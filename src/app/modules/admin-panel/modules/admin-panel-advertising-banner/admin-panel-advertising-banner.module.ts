import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './ui/pages/home-page/home-page.component';
import {AdminPanelAdvertisingBannerRoutingModule} from './admin-panel-advertising-banner-routing.module';
import {AdminPanelAdvertisingBannerComponent} from './admin-panel-advertising-banner.component';
import {CreatePageComponent} from './ui/pages/create-page/create-page.component';
import {UpdatePageComponent} from './ui/pages/update-page/update-page.component';
import {ViewPageComponent} from './ui/pages/view-page/view-page.component';
import {FormComponent} from './ui/components/form/form.component';
import {AdvertisingBannerService} from './shared/services/advertising-banner.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {KarbaCoreModule} from '../../shared/modules/karba-core/karba-core.module';

@NgModule({
  declarations: [
    AdminPanelAdvertisingBannerComponent,
    HomePageComponent,
    CreatePageComponent,
    UpdatePageComponent,
    ViewPageComponent,
    FormComponent
  ],
  providers: [
    AdvertisingBannerService
  ],
  exports: [
    HomePageComponent
  ],
    imports: [
        CommonModule,
        AdminPanelAdvertisingBannerRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        NgSelectModule,
        KarbaCoreModule
    ]
})
export class AdminPanelAdvertisingBannerModule { }
