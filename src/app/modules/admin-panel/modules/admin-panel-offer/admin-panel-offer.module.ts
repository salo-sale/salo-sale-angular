import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelOfferComponent } from './admin-panel-offer.component';
import { HomePageComponent } from './ui/pages/home-page/home-page.component';
import {AdminPanelOfferRoutingModule} from './admin-panel-offer-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OfferService} from './shared/services/offer.service';
import { CreatePageComponent } from './ui/pages/create-page/create-page.component';
import { FormComponent } from './ui/components/form/form.component';
import { UpdatePageComponent } from './ui/pages/update-page/update-page.component';
import { ViewPageComponent } from './ui/pages/view-page/view-page.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {ImageCropperModule} from 'ngx-image-cropper';
import {AdminPanelPointOfSaleModule} from '../admin-panel-point-of-sale/admin-panel-point-of-sale.module';
import { CodeComponent } from './ui/components/code/code.component';
import {LinkComponent} from './ui/components/link/link.component';
import {AdminPanelAdvertisingBannerModule} from '../admin-panel-advertising-banner/admin-panel-advertising-banner.module';
import {AdminPanelModule} from '../../admin-panel.module';
import {LocalityListModule} from '../../shared/modules/locality-list/locality-list.module';
import { ValidityComponent } from './ui/components/validity/validity.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularEditorModule} from "@kolkov/angular-editor";
import {PipeIsUndefinedOrNullModule} from "../../shared/modules/pipe-is-undefined-or-null/pipe-is-undefined-or-null.module";
import {KarbaCoreModule} from '../../shared/modules/karba-core/karba-core.module';

@NgModule({
  declarations: [
    AdminPanelOfferComponent,
    HomePageComponent,
    CreatePageComponent,
    FormComponent,
    UpdatePageComponent,
    ViewPageComponent,
    CodeComponent,
    LinkComponent,
    ValidityComponent
  ],
  providers: [
    OfferService
  ],
  exports: [
    HomePageComponent
  ],
    imports: [
        CommonModule,
        AdminPanelOfferRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        ImageCropperModule,
        AdminPanelPointOfSaleModule,
        AdminPanelAdvertisingBannerModule,
        LocalityListModule,
        NgbModule,
        AngularEditorModule,
        PipeIsUndefinedOrNullModule,
        KarbaCoreModule,
    ]
})
export class AdminPanelOfferModule {

  constructor(
    private offerService: OfferService
  ) {
    offerService.init();
  }

}
