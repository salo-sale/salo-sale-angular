import {Component, Input, OnInit} from '@angular/core';
import {OfferModel} from '../../../shared/models/offer.model';
import {AppService} from '../../../../../../../shared/services/app.service';
import {environment} from '../../../../../../../../environments/environment';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {OfferService} from '../../../shared/services/offer.service';
import {isNullOrUndefined} from 'util';
import {ActivatedRoute, Router} from '@angular/router';
import {CompanyModel} from '../../../../admin-panel-company/shared/models/company.model';
import {CompanyService} from '../../../../admin-panel-company/shared/services/company.service';

@Component({
  selector: 'app-admin-panel-offer-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  id = 'offer-form';

  public model: OfferModel;
  submitted: boolean;
  private loader: any;
  loadingCompanies = false;
  companies: CompanyModel[];

  constructor(
    private activatedRoute: ActivatedRoute,
    public appService: AppService,
    private router: Router,
    public offerService: OfferService,
    public companyService: CompanyService
  ) {

    this.submitted = false;

    if (isNullOrUndefined(offerService.document)) {

      this.model = new OfferModel();

    } else {

      this.model = offerService.document;
      this.model.initForm();

    }

    console.log(this.model);
    console.log(this.model.tags);

  }

  public tags: {id: number, name: string}[];
  useLimit: boolean;

  ngOnInit() {

    this.loader = document.querySelector('#app-admin-panel-offer-module-form-component .loader');

    this.loader.classList.remove('d-none');

    this.useLimit = false;

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      if (isNullOrUndefined(params.id)) {

        if (isNullOrUndefined(params.companyId)) {

          await this.companyService.getCompanies(true).then((result) => {

            this.companies = result;
            this.initEventOnNgSelect();

          });

        } else {

          this.model.companyId = Number(params.companyId);

          await this.companyService.getCompany(params.companyId).then((result) => {

            this.model.company = result;

          });
        }

      } else {

        await this.offerService.getOffer(params.id).then((result) => {

          this.model = this.offerService.document;

        });

      }

      this.tags = this.appService.listsOfTags.find((list) => list.name === environment.defaultDate.objects.offer).tags.map((tag) => {

        return {
          id: Number(tag.id),
          name: tag.name
        };

      });

      this.model.initForm();

    });

    this.loader.classList.add('d-none');

  }

  changed(data) {

    console.log(data);

    // this.model.tags = data.value.map(value => parseInt(value));

  }


  saveDocument(afterSaveGoToHome = false, afterSaveGoToNextNewDocument = false) {

    this.submitted = true;

    if (this.model.validForm) {

      if (!this.useLimit) {

        this.model.limit = null;

      }

      this.offerService.saveOffer(this.model).then((result) => {

        if (result) {

          this.model = new OfferModel();

          if (afterSaveGoToHome) {

            this.router.navigate(['admin', 'offer']);

          } else if (afterSaveGoToNextNewDocument) {

            this.model = new OfferModel();
            this.model.initForm();
            this.router.navigate(['admin', 'offer', 'create']);

          } else {


            this.router.navigate(['admin', 'offer', 'view', result]);

          }

        }

      });

    } else {

      console.error(this.model.formGroup);

    }

  }

  changeType(event) {

    this.loader.classList.remove('d-none');

    switch (this.model.formGroup.controls['type'].value) {

      case 1:
        this.tags = this.appService.listsOfTags.find((list) => list.name === environment.defaultDate.objects.offer).tags.map((tag) => {

          return {
            id: tag.id,
            name: tag.name
          };

        });
        break;
      case 2:
        this.tags = this.appService.listsOfTags.find((list) => list.name === environment.defaultDate.objects.offer).tags.map((tag) => {

          return {
            id: tag.id,
            name: tag.name
          };

        });
        break;
      case 3:
        this.tags = this.appService.listsOfTags.find((list) => list.name === environment.defaultDate.objects.offer).tags.map((tag) => {

          return {
            id: tag.id,
            name: tag.name
          };

        });
        break;
      case 4:
        this.tags = this.appService.listsOfTags.find((list) => list.name === environment.defaultDate.objects.event).tags.map((tag) => {

          return {
            id: tag.id,
            name: tag.name
          };

        });
        break;

    }

    this.loader.classList.add('d-none');

  }

  private initEventOnNgSelect() {

    setTimeout(() => {
      document.getElementById('company-' + this.id).querySelector('input').addEventListener('input', function (evt) {
        console.log(evt.target.value);
        if (evt.target.value.length > 1) {

          const finedIndex = this.companies.findIndex((c) => c.name.toLocaleLowerCase().search(evt.target.value.toLowerCase()) > -1);

          if (finedIndex === -1) {

            this.loadingCompanies = true;
            this.companyService.filter.search = evt.target.value;

            this.companyService.getCompanies(true).then((documentList) => {

              this.companies = documentList;
              this.loadingCompanies = false;

            });

          }

        }
      }.bind(this));


    }, 250);

  }

}
