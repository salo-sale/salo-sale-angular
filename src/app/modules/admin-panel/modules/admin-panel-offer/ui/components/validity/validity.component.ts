import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {OfferService} from '../../../shared/services/offer.service';
import {CodeModel} from '../../../shared/models/code.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ValidityOfferModel} from '../../../shared/models/validity-offer.model';
import {isNullOrUndefined} from 'util';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-offer-validity-component',
  templateUrl: './validity.component.html',
  styleUrls: ['./validity.component.scss']
})
export class ValidityComponent implements OnInit {
  id = 'offer-validity';
  pageId = 'app-admin-offer-validity-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;
  public offerService: OfferService;
  formGroup: FormGroup;
  selected: ValidityOfferModel;

  constructor(
    @Inject(forwardRef(() => OfferService)) offerService,
    private deviceService: DeviceDetectorService
  ) {


    this.isDesktop = deviceService.isDesktop();
    this.offerService = offerService;

  }

  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filterValidity.total);

    if (this.filterValidity.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  get validityList(): ValidityOfferModel[] {

    return this.offerService.validityList;

  }

  get filterValidity() {

    return this.offerService.filterValidity;

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.offerService.filter.resetPage();

    }

    this.offerService.getValidityList(true).then(() => {

      if (initPageList) {

        this.offerService.filterValidity.initPageList();

      }

      this.loader(false);

    });

  }

  ngOnInit() {

    this.initHideBody = true;
    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  initFormGroup() {

    if (isNullOrUndefined(this.selected)) {
      this.selected = new ValidityOfferModel();
    }

    this.formGroup = new FormBuilder().group({
      validityDate: [
        isNullOrUndefined(this.selected.validityDate) ? null : {year: this.selected.validityDate.getFullYear(), month: this.selected.validityDate.getMonth() + 1, day: this.selected.validityDate.getDate()}
      ],
      openTime: [
        this.selected.openTime
      ],
      closeTime: [
        this.selected.closeTime
      ],
      weekday: [
        this.selected.weekday
      ],
      canUse: [
        this.selected.canUse
      ],
      isRange: [
        this.selected.isRange
      ],
      isFrom: [
        this.selected.isFrom
      ],
      offerId: [
        this.offerService.document.id
      ]
    });

  }

  save() {

    console.log(this.selected);
    console.log(this.formGroup.valid);

    if (this.formGroup.valid) {

      const object = new ValidityOfferModel().deserialize(this.formGroup.value, this.selected._isNew);
      object.id = this.selected.id;
      console.log(object);

      this.offerService.saveValidity(object).then(() => {

        this.doFiltering();

      });

    }

  }

}
