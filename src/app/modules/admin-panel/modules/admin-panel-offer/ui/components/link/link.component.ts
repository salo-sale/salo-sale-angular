import {Component, forwardRef, Inject, Input, OnInit} from '@angular/core';
import {OfferService} from '../../../shared/services/offer.service';
import {OfferModel} from '../../../shared/models/offer.model';
import {CodeModel} from '../../../shared/models/code.model';
import {LinkModel} from '../../../shared/models/link.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CodeStatusEnum} from '../../../../../../../shared/enums/status/code.status';

@Component({
  selector: 'app-admin-panel-offer-link-component',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent implements OnInit {
  id = 'offer-links';
  pageId = 'app-admin-offer-links-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  public offerService: OfferService;
  addLinkFormGroup: FormGroup;

  constructor(
    @Inject(forwardRef(() => OfferService)) offerService,
  ) {

    this.offerService = offerService;

  }

  getStatusName(status: number) {
    return CodeStatusEnum[status];
  }

  get offerLinks(): LinkModel[] {

    return this.offerService.offerLinks;

  }

  get filterLink() {

    return this.offerService.filterLink;

  }

  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filterLink.total);

    if (this.filterLink.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.offerService.filter.resetPage();

    }

    this.offerService.getLinks(true).then(() => {

      if (initPageList) {

        this.offerService.filterLink.initPageList();

      }

      this.loader(false);

    });

  }

  ngOnInit() {

    this.initHideBody = true;
    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  initLinkFormGroup() {

    this.addLinkFormGroup = new FormBuilder().group({
      status: [
        1,
      ],
      link: [
        ''
      ],
      offerId: [
        this.offerService.document.id
      ]
    });

  }

  saveLink() {

    if (this.addLinkFormGroup.valid) {

      this.offerService.saveLink(new LinkModel().deserialize(this.addLinkFormGroup.value)).then(() => {

        this.doFiltering();

      });

    }

  }

}
