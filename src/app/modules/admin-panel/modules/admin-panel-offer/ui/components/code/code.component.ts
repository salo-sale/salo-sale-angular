import {Component, forwardRef, Inject, Input, OnInit} from '@angular/core';
import {OfferService} from '../../../shared/services/offer.service';
import {OfferModel} from '../../../shared/models/offer.model';
import {CodeModel} from '../../../shared/models/code.model';
import {isNullOrUndefined} from 'util';
import {FormBuilder, FormGroup} from '@angular/forms';
import { CodeStatusEnum } from 'src/app/shared/enums/status/code.status';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-offer-code-component',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.scss']
})
export class CodeComponent implements OnInit {
  id = 'offer-codes';
  pageId = 'app-admin-offer-codes-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;
  public offerService: OfferService;
  addCodeFormGroup: FormGroup;

  constructor(
    @Inject(forwardRef(() => OfferService)) offerService,
    private deviceService: DeviceDetectorService
  ) {


    this.isDesktop = deviceService.isDesktop();
    this.offerService = offerService;

  }

  get codes(): CodeModel[] {

    return this.offerService.offerCodes;

  }

  getStatusName(status: number) {
    return CodeStatusEnum[status];
  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.offerService.filter.resetPage();

    }

    this.offerService.getCodes(true).then(() => {

      if (initPageList) {

        this.offerService.filterCode.initPageList();

      }

      this.loader(false);
    });

  }

  ngOnInit() {

    this.initHideBody = true;
    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  changeStatus(status: number, code: CodeModel) {
    code.status = status;
    this.offerService.changeStatusCode(code).then(async () => {

      await this.offerService.getCodes();

    });

  }

  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filterCode.total);

    if (this.filterCode.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  initCodeFormGroup() {

    this.addCodeFormGroup = new FormBuilder().group({
      status: [
        1,
      ],
      code: [
        ''
      ],
      offerId: [
        this.offerService.document.id
      ]
    });

  }

  get filterCode() {

    return this.offerService.filterCode;

  }

  saveCode() {

    if (this.addCodeFormGroup.valid) {

      this.offerService.saveCode(new CodeModel().deserialize(this.addCodeFormGroup.value)).then(() => {

        this.doFiltering();

      });

    }

  }

}
