import {OfferLanguageInterface} from './interfaces/offer-language.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {TagModel} from '../../../../shared/models/tag.model';

export class OfferLanguageModel implements
  OfferLanguageInterface,
  DeserializableInterface<OfferLanguageInterface>,
  SerializableInterface<OfferLanguageInterface> {

  description: string;
  languageCode: string;
  descriptionRequirements: string;
  tags: TagModel[];
  title: string;
  id: number;

  _isNew: boolean;

  constructor() {

    this._isNew = true;

  }

  deserialize(input: OfferLanguageInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): OfferLanguageInterface {

    const object = Object.assign({} as OfferLanguageInterface, this);
    delete object._isNew;

    return object;

  }


}
