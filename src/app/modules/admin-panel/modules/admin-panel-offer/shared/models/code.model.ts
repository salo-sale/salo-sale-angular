import {ValidityOfferInterface} from './interfaces/validity-offer.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {CodeInterface} from './interfaces/code.interface';
import {OfferInterface} from './interfaces/offer.interface';

export class CodeModel implements CodeInterface, DeserializableInterface<CodeInterface>, SerializableInterface<CodeInterface> {

  code: string;
  createdAt: string;
  offerId: number;
  status: number;
  updatedAt: string;


  _isNew: boolean;

  constructor() {
    this.initModel();
  }


  deserialize(input: CodeInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): CodeInterface {

    const object = Object.assign({} as CodeInterface, this);
    delete object._isNew;

    return object;

  }

  public initModel() {

    this._isNew = true;

  }

}
