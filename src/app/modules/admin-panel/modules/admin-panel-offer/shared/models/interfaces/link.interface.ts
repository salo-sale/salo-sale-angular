export interface LinkInterface {

  link: string;
  offerId: number;
  status: number;
  createdAt: string;
  updatedAt: string;

}
