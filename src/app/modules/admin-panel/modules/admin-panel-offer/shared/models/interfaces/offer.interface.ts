import {ValidityOfferInterface} from './validity-offer.interface';
import {OfferLanguageInterface} from './offer-language.interface';
import {CompanyInterface} from '../../../../admin-panel-company/shared/models/interfaces/company.interface';
import {OfferLocalityInterface} from './offer-locality.interface';
import {ImageInterface} from '../../../../../shared/models/interfaces/image.interface';

export interface OfferInterface {

  id: number;
  price: number;
  limit: number;
  numberOfViews: number;
  amountOfUses: number;
  validityOffers?: ValidityOfferInterface[];
  income: number;
  accessForUser: number;
  type: number;
  status: number;
  companyId: number;
  ikointForTake: number;
  accessForUse: number;
  languages: OfferLanguageInterface[];
  updatedAt?: string;
  company?: CompanyInterface;
  tags: number[];
  pointsOfSale?: string[];
  offerLocalityList?: OfferLocalityInterface[];
  bannerUrl?: ImageInterface[];
  logoUrl?: ImageInterface;

}
