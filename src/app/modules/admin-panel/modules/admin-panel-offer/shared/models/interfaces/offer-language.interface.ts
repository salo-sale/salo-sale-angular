import {TagInterface} from '../../../../../shared/models/interfaces/tag.interface';

export interface OfferLanguageInterface {

  languageCode: string;
  title: string;
  description: string;
  descriptionRequirements: string;
  tags: TagInterface[];
  id: number;

}
