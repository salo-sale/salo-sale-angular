export interface CodeInterface {

  code: string;
  offerId: number;
  status: number;
  createdAt: string;
  updatedAt: string;

}
