import { Injectable } from '@angular/core';
import {AppService} from '../../../../../../shared/services/app.service';
import {RequestConfigurationModel} from '../../../../../../shared/models/request-configuration.model';
import {LocalStorageTool} from '../../../../../../shared/tools/local-storage.tool';
import {FilterModel} from '../models/filter.model';
import {NotificationStyle} from '../../../../shared/modules/notification/shared/enums/notification-style.enum';
import {NotificationService} from '../../../../shared/modules/notification/shared/services/notification.service';
import {NotificationModel} from '../models/notification.model';
import {NotificationInterface} from '../models/interfaces/notification.interface';


@Injectable({
  providedIn: 'root'
})
export class AppAdminPanelNotificationService {

  public filter: FilterModel;
  public documents: NotificationModel[];
  private initialize: boolean;
  document: NotificationModel;

  constructor(
    public appService: AppService,
    public notificationService: NotificationService
  ) {

    this.init();

  }

  async getDocuments(reset: boolean = false): Promise<NotificationModel[]> {

    return new Promise<NotificationModel[]>(async (resolve, reject) => {

      if (this.documents.length === 0 || reset) {

        const filter = this.filter.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'notifications',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: NotificationInterface[],
          total: number
        }) => {

          this.documents = [];

          this.documents = data.models.map((company) => new NotificationModel().deserialize(company));
          this.filter.total = data.total;

          resolve(this.documents);

        });

      } else {

        resolve(this.documents);

      }

    });

  }

  init() {

    this.filter = new FilterModel();
    this.documents = [];
    this.document = null;

    if (!this.initialize) {

      // this.documents = LocalStorageTool.getCompanies();
      //
      // this.filter = LocalStorageTool.getFilterCompany();
      this.filter.initPageList();

      this.initialize = true;

    }

  }

  async saveDocument(model: NotificationModel): Promise<boolean | number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'notifications',
        subUrl: model._isNew ? '' : '/' + model.id.toString(),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Заклад',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Заклад',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async getDocument(id: string): Promise<NotificationModel> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'notifications',
        subUrl: '/' + id.toString()
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((company: NotificationInterface) => {

        console.log(company);
        this.document = new NotificationModel().deserialize(company);

        resolve(this.document);

      });

    });

  }

  sendPushNotification(id: string) {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'notifications',
        subUrl: '/' + id + '/push-notification',
        params: {
          type: 'post',
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((success: number) => {

        resolve(success === 1);

      });

    });

  }
}
