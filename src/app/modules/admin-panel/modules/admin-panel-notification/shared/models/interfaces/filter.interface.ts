export interface FilterInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;
  expand: string;
  status: number;
  search: string;
  companyId: string;

}
