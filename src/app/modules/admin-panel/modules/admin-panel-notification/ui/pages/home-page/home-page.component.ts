import {Component, forwardRef, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {CompanyStatusEnum} from '../../../../../../../shared/enums/status/company.status.enum';
import {NotificationModel} from '../../../shared/models/notification.model';
import {AppAdminPanelNotificationService} from '../../../shared/services/app-admin-panel-notification.service';
import {FilterModel} from '../../../shared/models/filter.model';
import {NotificationStatusEnum} from '../../../../../../../shared/enums/status/notification.status.enum';
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-admin-notification-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  id = 'notifications';
  pageId = 'app-admin-notifications-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  @Input()
  companyId: string;

  public adminService: AdminPanelService;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public appAdminPanelNotificationService: AppAdminPanelNotificationService
  ) {

    adminService.titlePage = 'Повідомлення';
    this.adminService = adminService;

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filter.total);

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  clearSearch() {

    this.appAdminPanelNotificationService.filter.search = '';
    this.doFiltering();

  }

  get filter(): FilterModel {

    return this.appAdminPanelNotificationService.filter;

  }

  get documents(): NotificationModel[] {

    return this.appAdminPanelNotificationService.documents;

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.appAdminPanelNotificationService.filter.resetPage();

    }

    this.appAdminPanelNotificationService.getDocuments(true).then(() => {

      if (initPageList) {

        this.appAdminPanelNotificationService.filter.initPageList();

      }

      this.loader(false);

    });

  }

  statusName(status) {

    return CompanyStatusEnum[status];

  }

  ngOnInit() {

    if (isNullOrUndefined(this.companyId)) {

      this.initHideBody = false;

    } else {

      this.initHideBody = true;
      this.filter.companyId = this.companyId;

    }

    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  selectDocument(document: NotificationModel) {

    this.appAdminPanelNotificationService.document = document;

  }

  getStatusName(status: number) {
    return NotificationStatusEnum[status];
  }

  ngOnDestroy(): void {
    this.appAdminPanelNotificationService.init();
  }
}
