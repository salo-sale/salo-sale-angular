import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {ImageInterface} from '../../../../shared/models/interfaces/image.interface';
import {QrCodeInterface} from './interfaces/qr-code.interface';
import {environment} from '../../../../../../../environments/environment';

export class QrCodeModel implements QrCodeInterface, DeserializableInterface<QrCodeInterface>, SerializableInterface<QrCodeInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  id: number;
  company_id: number;
  code_name: string;
  amount_of_uses: number;
  video_id: string;
  authentication: string;
  ssid: string;
  password: string;
  first_name: string;
  last_name: string;
  email: string;
  body: string;
  subject: string;
  phone: string;
  sound: string;
  video_phone: string;
  note: string;
  birthday: string;
  address: string;
  url: string;
  nick: string;
  title: string;
  lat: number;
  lng: number;
  altitude: number;
  amount: string;
  summary: string;
  start: string;
  end: string;
  type: number | string;
  status: number;
  updated_at: string;
  created_at: string;

  _isNew: boolean;

  // 0 ALL

  // 1 DEFAULT
  // 2 YOUTUBE
  // 3 WIFI
  // 4 V_CARD
  // 5 SMS
  // 6 PHONE
  // 7 MMS
  // 8 ME_CARD
  // 9 GEO
  // 10 BTC
  // 11 BOOK_MARK
  // 12 MAIL_TO
  // 13 MAIL_MESSAGE
  // 14 I_CARD

  _activeFieldByType: {
    'company_id': number[];
    'code_name': number[];
    'video_id': number[];
    'authentication': number[];
    'ssid': number[];
    'password': number[];
    'first_name': number[];
    'last_name': number[];
    'email': number[];
    'body': number[];
    'subject': number[];
    'phone': number[];
    'sound': number[];
    'video_phone': number[];
    'note': number[];
    'birthday': number[];
    'address': number[];
    'url': number[];
    'nick': number[];
    'title': number[];
    'lat': number[];
    'lng': number[];
    'altitude': number[];
    'amount': number[];
    'summary': number[];
    'start': number[];
    'end': number[];
    'type': number[];
    'status': number[];
  } = {

    'company_id': [0],
    'code_name': [0],
    'video_id': [2],
    'authentication': [3],
    'ssid': [3],
    'password': [3],
    'first_name': [8, 4],
    'last_name': [8, 4],
    'email': [8, 4, 13, 12],
    'body': [13, 7],
    'subject': [13],
    'phone': [8, 5, 7, 6],
    'sound': [8],
    'video_phone': [8],
    'note': [8],
    'birthday': [8],
    'address': [10, 8],
    'url': [11, 1, 8],
    'nick': [8],
    'title': [10, 11],
    'lat': [9],
    'lng': [9],
    'altitude': [9],
    'amount': [10],
    'summary': [14],
    'start': [14],
    'end': [14],
    'type': [0],
    'status': [0],

  };

  _listOfFields: {name: string, label: string, placeholder: string, isShow: boolean}[] = [

    {
      name: 'company_id',
      label: '',
      placeholder: '',
      isShow: false
    },
    {
      name: 'code_name',
      label: 'Назва',
      placeholder: 'Меню кухні',
      isShow: true
    },
    {
      name: 'video_id',
      label: 'Ідентифікатор відео',
      placeholder: '000000000',
      isShow: true
    },
    {
      name: 'authentication',
      label: 'Тип авторизації',
      placeholder: 'WEP, WPA, або nopass або пусте поле',
      isShow: true
    },
    {
      name: 'ssid',
      label: 'Назва мережі',
      placeholder: 'Salo-Sale',
      isShow: true
    },
    {
      name: 'password',
      label: 'Пароль',
      placeholder: '*******',
      isShow: true
    },
    {
      name: 'first_name',
      label: 'Ім`я',
      placeholder: 'Іван',
      isShow: true
    },
    {
      name: 'last_name',
      label: 'Прізвище',
      placeholder: 'Шевченко',
      isShow: true
    },
    {
      name: 'email',
      label: 'Електронна пошта',
      placeholder: 'https://salo-sale.com/companies/1',
      isShow: true
    },
    {
      name: 'body',
      label: 'Текст',
      placeholder: 'Напишіть що небудь',
      isShow: true
    },
    {
      name: 'subject',
      label: 'Тема',
      placeholder: 'Напишіть тут тему листа',
      isShow: true
    },
    {
      name: 'phone',
      label: 'Телефон',
      placeholder: '+38000000000',
      isShow: true
    },
    {
      name: 'sound',
      label: 'Назва милодії',
      placeholder: 'Впишіть тут назву милодії для контакту',
      isShow: true
    },
    {
      name: 'video_phone',
      label: 'Номер відео дзвінка',
      placeholder: 'Тут впишіть номер для відео дзвінка',
      isShow: true
    },
    {
      name: 'note',
      label: 'Нотатка',
      placeholder: 'Щоб хочете тут пишіть',
      isShow: true
    },
    {
      name: 'birthday',
      label: 'Дата нарождення',
      placeholder: '00.00.0000',
      isShow: true
    },
    {
      name: 'address',
      label: 'Адреса',
      placeholder: '',
      isShow: true
    },
    {
      name: 'url',
      label: 'Посилання',
      placeholder: 'https://salo-sale.com',
      isShow: true
    },
    {
      name: 'nick',
      label: 'Нікнейм',
      placeholder: 'SALOMAN',
      isShow: true
    },
    {
      name: 'title',
      label: 'Назва',
      placeholder: 'SALO_SALE_POINT',
      isShow: true
    },
    {
      name: 'lat',
      label: 'Широта',
      placeholder: '00.0000',
      isShow: true
    },
    {
      name: 'lng',
      label: 'Довгота',
      placeholder: '00.0000',
      isShow: true
    },
    {
      name: 'altitude',
      label: 'Приближення (зум)',
      placeholder: '16',
      isShow: true
    },
    {
      name: 'amount',
      label: 'Сума',
      placeholder: '00.00',
      isShow: true
    },
    {
      name: 'summary',
      label: 'Сумарність',
      placeholder: 'Може бути назва або цифри',
      isShow: true
    },
    {
      name: 'start',
      label: 'Дата початку',
      placeholder: '00.00.0000 00:00',
      isShow: true
    },
    {
      name: 'end',
      label: 'Дата кінця',
      placeholder: '00.00.0000 00:00',
      isShow: true
    },
    {
      name: 'type',
      label: 'Тип',
      placeholder: '',
      isShow: false
    },
    {
      name: 'status',
      label: '',
      placeholder: '',
      isShow: false
    },

  ];

  constructor() {

    this._isNew = true;
    this.status = 1;
    this.initForm();

  }

  checkFieldForType(field: string) {

    if (!this._activeFieldByType[field].includes(0)) {

      return this._activeFieldByType[field].includes(Number(this.formGroup.get('type').value.toString()));

    }

    return true;

  }

  deserialize(input: QrCodeInterface): this {

    this._isNew = false;

    const object = Object.assign(this, input);

    object.type = Object.keys(environment.typeMapQrCode).findIndex((k) => k === object.type.toString());

    return object;

  }

  serialize(): QrCodeInterface {

    const object = Object.assign({} as QrCodeInterface, this);
    delete object._isNew;
    delete object.createdAt;
    delete object.updatedAt;
    delete object.formBuilder;
    delete object._activeFieldByType;
    delete object._listOfFields;

    object.type = Object.keys(environment.typeMapQrCode)[object.type];

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      const value = this.formGroup.value;

      this.company_id = value.company_id;
      this.code_name = value.code_name;
      this.video_id = value.video_id;
      this.authentication = value.authentication;
      this.ssid = value.ssid;
      this.password = value.password;
      this.first_name = value.first_name;
      this.last_name = value.last_name;
      this.email = value.email;
      this.body = value.body;
      this.subject = value.subject;
      this.phone = value.phone;
      this.sound = value.sound;
      this.video_phone = value.video_phone;
      this.note = value.note;
      this.birthday = value.birthday;
      this.address = value.address;
      this.url = value.url;
      this.nick = value.nick;
      this.title = value.title;
      this.lat = value.lat;
      this.lng = value.lng;
      this.altitude = value.altitude;
      this.amount = value.amount;
      this.summary = value.summary;
      this.start = value.start;
      this.end = value.end;
      this.type = value.type;
      this.status = value.status;

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    this.formBuilder = new FormBuilder().group({
      company_id: [
        this.company_id,
        [
          Validators.required
        ]
      ],
      code_name: [
        this.code_name,
        [
          Validators.required
        ]
      ],
      video_id: [
        this.video_id
      ],
      authentication: [
        this.authentication
      ],
      ssid: [
        this.ssid
      ],
      password: [
        this.password
      ],
      first_name: [
        this.first_name
      ],
      last_name: [
        this.last_name
      ],
      email: [
        this.email
      ],
      body: [
        this.body
      ],
      subject: [
        this.subject
      ],
      phone: [
        this.phone
      ],
      sound: [
        this.sound
      ],
      video_phone: [
        this.video_phone
      ],
      note: [
        this.note
      ],
      birthday: [
        this.birthday
      ],
      address: [
        this.address
      ],
      url: [
        this.url
      ],
      nick: [
        this.nick
      ],
      title: [
        this.title
      ],
      lat: [
        this.lat
      ],
      lng: [
        this.lng
      ],
      altitude: [
        this.altitude
      ],
      amount: [
        this.amount
      ],
      summary: [
        this.summary
      ],
      start: [
        this.start
      ],
      end: [
        this.end
      ],
      type: [
        this.type,
        [
          Validators.required
        ]
      ],
      status: [
        this.status,
        [
          Validators.required
        ]
      ]
    });

  }

  initModel() {

    this.type = 1;
    this.status = 1;

    return this;

  }

}
