export interface ContactInterface {

  id: number;
  companyId: number;
  name: string;
  value: string;
  type: number;
  status: number;

}
