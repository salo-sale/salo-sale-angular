export interface FilterContactInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;
  expand: string;
  status: number;

}
