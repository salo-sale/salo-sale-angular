import {ImageInterface} from '../../../../../shared/models/interfaces/image.interface';

export interface QrCodeInterface {

  id: number;
  company_id: number;
  code_name: string;
  amount_of_uses: number;
  video_id: string;
  authentication: string;
  ssid: string;
  password: string;
  first_name: string;
  last_name: string;
  email: string;
  body: string;
  subject: string;
  phone: string;
  sound: string;
  video_phone: string;
  note: string;
  birthday: string;
  address: string;
  url: string;
  nick: string;
  title: string;
  lat: number;
  lng: number;
  altitude: number;
  amount: string;
  summary: string;
  start: string;
  end: string;
  type: number | string;
  status: number;
  updated_at: string;
  created_at: string;

  companyId?: number;
  codeName?: string;
  amountOfUses?: number;
  videoId?: string;
  firstName?: string;
  lastName?: string;
  videoPhone?: string;
  updatedAt?: string;
  createdAt?: string;

}
