import {ContactInterface} from './interfaces/contact.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isNullOrUndefined} from "util";

export class ContactModel implements ContactInterface, DeserializableInterface<ContactInterface>, SerializableInterface<ContactInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  companyId: number;
  id: number;
  name: string;
  status: number;
  type: number;
  value: string;

  _isNew: boolean;

  constructor() {

    this.type = 0;
    this._isNew = true;

  }

  deserialize(input: ContactInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): ContactInterface {

    const object = Object.assign({} as ContactInterface, this);
    delete object._isNew;
    delete object.formBuilder;

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

          this[key] = this.formGroup.value[key];

        }

      });

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    console.log(this.status);

    this.formBuilder = new FormBuilder().group({
      name: [
        this.name
      ],
      type: [
        this.type,
        [
          Validators.required,
        ]
      ],
      value: [
        this.value,
        [
          Validators.required,
        ]
      ],
      status: [
        this._isNew ? 1 : Number(this.status.toString())
      ],
    });

  }

}
