import { Injectable } from '@angular/core';
import {AppService} from '../../../../../../shared/services/app.service';
import {RequestConfigurationModel} from '../../../../../../shared/models/request-configuration.model';
import {LocalStorageTool} from '../../../../../../shared/tools/local-storage.tool';
import {CompanyModel} from '../models/company.model';
import {FilterModel} from '../models/filter.model';
import {CompanyInterface} from '../models/interfaces/company.interface';
import {OfferModel} from '../../../admin-panel-offer/shared/models/offer.model';
import {NotificationStyle} from '../../../../shared/modules/notification/shared/enums/notification-style.enum';
import {NotificationService} from '../../../../shared/modules/notification/shared/services/notification.service';
import {ContactModel} from '../models/contact.model';
import {FilterContactModel} from '../models/filter-contact.model';
import {ContactInterface} from '../models/interfaces/contact.interface';
import {MemberModel} from '../models/member.model';
import {FilterMemberModel} from '../models/filter-member.model';
import {MemberInterface} from '../models/interfaces/member.interface';
import {map} from 'rxjs/operators';
import {QrCodeModel} from '../models/qr-code.model';
import {FilterQrCodeModel} from '../models/filter-qr-code.model';
import {QrCodeInterface} from '../models/interfaces/qr-code.interface';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  public filter: FilterModel;
  public qrCodes: QrCodeModel[];
  public companies: CompanyModel[];
  private initialize: boolean;
  selectedCompany: CompanyModel;
  contacts: ContactModel[];
  members: MemberModel[];
  filterContact: FilterContactModel;
  filterMember: FilterMemberModel;
  filterQrCode: FilterQrCodeModel;

  constructor(
    public appService: AppService,
    public notificationService: NotificationService
  ) {

    this.filter = new FilterModel();
    this.filterContact = new FilterContactModel();
    this.filterMember = new FilterMemberModel();
    this.filterQrCode = new FilterQrCodeModel();
    this.contacts = [];
    this.members = [];
    this.companies = [];
    this.qrCodes = [];
    this.selectedCompany = null;

  }

  async getCompanies(reset: boolean = false): Promise<CompanyModel[]> {

    return new Promise<CompanyModel[]>(async (resolve, reject) => {

      if (this.companies.length === 0 || reset) {

        const filter = this.filter.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'companies',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: CompanyInterface[],
          total: number
        }) => {

          this.companies = [];

          this.companies = data.models.map((company) => new CompanyModel().deserialize(company));
          this.filter.total = data.total;

          // LocalStorageTool.setCompanies(this.companies);
          // LocalStorageTool.setFilterCompany(this.filter);

          resolve(this.companies);

        });

      } else {

        resolve(this.companies);

      }

    });

  }

  async getContacts(reset: boolean = false, companyId: number = null): Promise<ContactModel[]> {

    return new Promise<ContactModel[]>(async (resolve, reject) => {

      console.log(this.contacts);

      if (this.contacts.length === 0 || reset) {

        const filter = this.filterContact.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'company',
          subUrl: companyId + '/contacts',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: ContactInterface[],
          total: number
        }) => {

          this.contacts = [];

          this.contacts = data.models.map((company) => new ContactModel().deserialize(company));
          this.filterContact.total = data.total;

          // LocalStorageTool.setCompanies(this.contacts);
          // LocalStorageTool.setFilterCompany(this.filter);

          resolve(this.contacts);

        });

      } else {

        resolve(this.contacts);

      }

    });

  }

  async deleteContact(id: number): Promise<boolean> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'company',
        subUrl: this.selectedCompany.id.toString() + '/contact/' + id.toString(),
        params: {
          type: 'delete'
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Контакт',
          body: 'Успішно видалений.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });

  }

  async saveContact(model: ContactModel): Promise<boolean | number> {

    return new Promise(async (resolve) => {

      console.log(model);

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'company',
        subUrl: model.companyId.toString() + '/contact' + (model._isNew ? '' : '/' + model.id.toString()),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Контакт',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async getMembers(reset: boolean = false, companyId: number = null): Promise<MemberModel[]> {

    return new Promise<MemberModel[]>(async (resolve, reject) => {

      console.log(this.contacts);

      if (this.contacts.length === 0 || reset) {

        const filter = this.filterMember.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'company',
          subUrl: companyId + '/members',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: MemberModel[],
          total: number
        }) => {

          this.members = [];

          if (data.total > 0) {
            this.members = data.models.map((company) => new MemberModel().initModel().deserialize(company));
          }

          this.filterMember.total = data.total;

          resolve(this.members);

        });

      } else {

        resolve(this.members);

      }

    });

  }

  async deleteMember(id: number): Promise<boolean> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'company',
        subUrl: this.selectedCompany.id.toString() + '/member/' + id.toString(),
        params: {
          type: 'delete'
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Контакт',
          body: 'Успішно видалений.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });

  }

  async saveMember(model: MemberModel): Promise<boolean | number> {

    return new Promise(async (resolve) => {

      console.log(model);

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'company',
        subUrl: model.companyId.toString() + '/member' + (model._isNew ? '' : '/' + model.id.toString()),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Контакт',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async getQrCodes(reset: boolean = false): Promise<QrCodeModel[]> {

    return new Promise<QrCodeModel[]>(async (resolve, reject) => {

      console.log(this.contacts);

      if (this.qrCodes.length === 0 || reset) {

        const filter = this.filterQrCode.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'qr-codes',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: QrCodeModel[],
          total: number
        }) => {

          this.members = [];

          if (data.total > 0) {
            this.qrCodes = data.models.map((qrCode) => new QrCodeModel().initModel().deserialize(qrCode));
          }

          this.filterQrCode.total = data.total;

          resolve(this.qrCodes);

        });

      } else {

        resolve(this.qrCodes);

      }

    });

  }

  async deleteQrCode(id: number): Promise<boolean> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'qr-codes',
        subUrl: '/' + id.toString(),
        params: {
          type: 'delete'
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Контакт',
          body: 'Успішно видалений.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });

  }

  async saveQrCode(model: QrCodeModel): Promise<boolean | number> {

    return new Promise(async (resolve) => {

      console.log(model);

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'qr-codes',
        subUrl: model._isNew ? '' : '/' + model.id.toString(),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((object: QrCodeInterface) => {

        console.log(object);

        this.notificationService.addToStream({
          title: 'QR code',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve((!isNullOrUndefined(object) && Object.keys(object).length > 0));

      }, error => {

        console.log(error);


        this.notificationService.addToStream({
          title: 'QR code',
          body: error.error,
          show: true,
          style: NotificationStyle.ERROR
        });

        resolve(false);

      });

    });

  }

  init() {

    if (!this.initialize) {

      // this.companies = LocalStorageTool.getCompanies();
      //
      // this.filter = LocalStorageTool.getFilterCompany();
      this.filter.initPageList();

      this.initialize = true;

    }

  }

  async saveCompany(model: CompanyModel): Promise<boolean | number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'companies',
        subUrl: model._isNew ? '' : '/' + model.id.toString(),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Заклад',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Заклад',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async getCompany(id: string): Promise<CompanyModel> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'companies',
        subUrl: '/' + id
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((company: CompanyInterface) => {

        console.log(company);
        this.selectedCompany = new CompanyModel().deserialize(company);

        resolve(this.selectedCompany);

      });

    });

  }

  async saveLogo(model: CompanyModel, image: File): Promise<boolean> {

    const formData = new FormData();

    formData.append('companyId', model.id.toString());
    formData.append('imageFile', image, '1');

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'company',
        subUrl: model.id.toString() + '/image',
        params: {
          type: 'post',
          body: formData
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Лого закладу',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Лого закладу',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });


  }
}
