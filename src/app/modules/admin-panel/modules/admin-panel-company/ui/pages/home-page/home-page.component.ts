import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {TagModel} from '../../../../../shared/models/tag.model';
import {CompanyService} from '../../../shared/services/company.service';
import {CompanyModel} from '../../../shared/models/company.model';
import {CompanyStatusEnum} from '../../../../../../../shared/enums/status/company.status.enum';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  id = 'companies';
  pageId = 'app-admin-companies-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;
  public isDesktop = false;

  public adminService: AdminPanelService;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public companyService: CompanyService,
    private deviceService: DeviceDetectorService
  ) {


    this.isDesktop = deviceService.isDesktop();
    adminService.titlePage = 'Заклади';
    this.adminService = adminService;

  }

  clearSearch() {

    this.companyService.filter.search = '';
    this.doFiltering();

  }

  get filter() {

    return this.companyService.filter;

  }

  get companies(): CompanyModel[] {

    return this.companyService.companies;

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.companyService.filter.resetPage();

    }

    this.companyService.getCompanies(true).then(() => {

      if (initPageList) {

        this.companyService.filter.initPageList();

      }

      this.loader(false);

    });

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filter.total);

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  statusName(status) {

    return CompanyStatusEnum[status];

  }

  ngOnInit() {

    this.initHideBody = false;
    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  selectCompany(company: CompanyModel) {

    this.companyService.selectedCompany = company;

  }
}
