import {Component, Input, OnInit} from '@angular/core';
import {ContactModel} from '../../../shared/models/contact.model';
import {CompanyService} from '../../../shared/services/company.service';
import {isNullOrUndefined, isNumber} from 'util';
import {environment} from '../../../../../../../../environments/environment';
import {CompanyModel} from '../../../shared/models/company.model';
import {MemberModel} from '../../../shared/models/member.model';
import { MemberStatusEnum } from 'src/app/shared/enums/status/member.status';
import {DeviceDetectorService} from 'ngx-device-detector';
import {MemberInterface} from '../../../shared/models/interfaces/member.interface';

@Component({
  selector: 'app-admin-panel-company-module-members-component',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  id = 'company-members';
  pageId = 'app-admin-company-members-home';

  public initialized = false;
  public initializedForm = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;
  @Input()
  companyId: number = null;
  selectedItem: MemberModel;

  constructor(
    public companyService: CompanyService,
    private deviceService: DeviceDetectorService
  ) {

    this.selectedItem = new MemberModel().initModel();
    this.selectedItem.companyId = this.companyId;
    this.selectedItem.initForm();
    this.initializedForm = true;
    this.isDesktop = deviceService.isDesktop();

  }

  get filterMember() {

    return this.companyService.filterMember;

  }

  getStatusName(status: number) {
    return MemberStatusEnum[status];
  }

  get members(): MemberModel[] {

    return this.companyService.members;

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    this.companyService.getMembers(true, this.companyId).then(() => {

      if (initPageList) {

        this.companyService.filterMember.initPageList();

      }

      this.loader(false);

    });

  }

  typeMember(role) {

    return isNullOrUndefined(role) ? environment.rolesCompany : environment.rolesCompany[Number(role)];

  }

  ngOnInit() {

    if (isNullOrUndefined(this.companyId)) {

      this.initHideBody = false;

    } else {

      this.initHideBody = true;

    }

    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filterMember.total);

    if (this.filterMember.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  initSelectedItem(selectedItem = new MemberModel().initModel()) {

    this.initializedForm = false;
    this.selectedItem = selectedItem;
    this.selectedItem.initForm();
    this.initializedForm = true;
    console.log(this.selectedItem.formGroup);

  }

  saveMember() {

    if (this.selectedItem.validForm) {

      console.log(this.selectedItem);

      this.selectedItem.companyId = this.companyId;
      this.companyService.saveMember(this.selectedItem).then((result) => {

        if (isNumber(result)) {

          this.doFiltering();

        }

      });
      console.log(this.selectedItem);

    }

  }

  deleteMember() {

    if (confirm('Чи напевно хочеш видалити?') === true) {


      if (!this.selectedItem._isNew) {

        this.companyService.deleteMember(this.selectedItem.id).then((result) => {

          this.doFiltering();
          this.initSelectedItem();

        });

      }

    }

  }

}
