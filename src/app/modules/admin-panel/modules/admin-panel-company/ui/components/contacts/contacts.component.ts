import {Component, forwardRef, Inject, Input, OnInit} from '@angular/core';
import {CompanyService} from '../../../shared/services/company.service';
import {CompanyModel} from '../../../shared/models/company.model';
import {ContactModel} from '../../../shared/models/contact.model';
import {isNullOrUndefined, isNumber} from 'util';
import {environment} from '../../../../../../../../environments/environment';
import { ContactStatusEnum } from 'src/app/shared/enums/status/contact.status';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-company-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  id = 'company-contacts';
  pageId = 'app-admin-company-contacts-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;

  @Input()
  companyId: number = null;
  selectedItem: ContactModel;

  constructor(
    public companyService: CompanyService,
    private deviceService: DeviceDetectorService
  ) {

    this.isDesktop = deviceService.isDesktop();
    this.selectedItem = new ContactModel();
    this.selectedItem.initForm();

  }

  get filterContact() {

    return this.companyService.filterContact;

  }

  get contacts(): ContactModel[] {

    return this.companyService.contacts;

  }

  getStatusName(status: number) {
    return ContactStatusEnum[status];
  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.companyService.filterContact.resetPage();

    }

    this.companyService.getContacts(true, this.companyId).then(() => {

      if (initPageList) {

        this.companyService.filterContact.initPageList();

      }

      this.loader(false);

    });

  }

  typeContact(type) {

    return isNullOrUndefined(type) ? environment.typeContact : environment.typeContact[Number(type)];

  }

  ngOnInit() {

    if (isNullOrUndefined(this.companyId)) {

      this.initHideBody = false;

    } else {

      this.initHideBody = true;

    }

    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filterContact.total);

    if (this.filterContact.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  selectCompany(company: CompanyModel) {

    this.companyService.selectedCompany = company;

  }

  initSelectedItem(selectedItem = new ContactModel()) {

    this.selectedItem = selectedItem;
    this.selectedItem.initForm();
    console.log(this.selectedItem.formGroup);

  }

  saveContact() {

    if (this.selectedItem.validForm) {

      console.log(this.selectedItem);

      this.selectedItem.companyId = this.companyId;
      this.companyService.saveContact(this.selectedItem).then((result) => {

        if (isNumber(result)) {

          this.doFiltering();

        }

      });
      console.log(this.selectedItem);

    }

  }

  deleteContact() {

    if (!this.selectedItem._isNew) {

      this.companyService.deleteContact(this.selectedItem.id).then((result) => {

        this.doFiltering();
        this.initSelectedItem();

      });

    }

  }

}
