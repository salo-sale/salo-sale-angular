import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPanelCompanyComponent } from './admin-panel-company.component';

describe('AdminPanelCompanyComponent', () => {
  let component: AdminPanelCompanyComponent;
  let fixture: ComponentFixture<AdminPanelCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPanelCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPanelCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
