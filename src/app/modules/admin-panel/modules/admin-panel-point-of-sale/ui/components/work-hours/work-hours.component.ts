import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {PointOfSaleService} from '../../../shared/services/point-of-sale.service';
import {WorkHoursModel} from '../../../shared/models/work-hours.model';
import {PointOfSaleStatusEnum} from '../../../../../../../shared/enums/status/point-of-sale.status.enum';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-point-of-sale-work-hours-component',
  templateUrl: './work-hours.component.html',
  styleUrls: ['./work-hours.component.scss']
})
export class WorkHoursComponent implements OnInit {
  id = 'point-of-sale-work-hours';
  pageId = 'app-admin-point-of-sale-work-hours-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;
  public pointOfSaleService: PointOfSaleService;
  formGroup: FormGroup;
  selected: WorkHoursModel;

  constructor(
    @Inject(forwardRef(() => PointOfSaleService)) pointOfSaleService,
    private deviceService: DeviceDetectorService
  ) {
    this.isDesktop = deviceService.isDesktop();

    this.pointOfSaleService = pointOfSaleService;

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  get workHoursList(): WorkHoursModel[] {

    return this.pointOfSaleService.workHoursList;

  }

  get filter() {

    return this.pointOfSaleService.filterWorkHours;

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.pointOfSaleService.filter.resetPage();

    }

    this.pointOfSaleService.getWorkHours(true).then(() => {

      if (initPageList) {

        this.pointOfSaleService.filterWorkHours.initPageList();

      }

      this.loader(false);

    });

  }

  getStatusName(status) {

    return PointOfSaleStatusEnum[status];

  }

  ngOnInit() {

    this.initHideBody = true;
    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  initFormGroup() {

    if (isNullOrUndefined(this.selected)) {
      this.selected = new WorkHoursModel();
    }

    this.formGroup = new FormBuilder().group({
      workDate: [
        isNullOrUndefined(this.selected.workDate) ? null : {year: this.selected.workDate.getFullYear(), month: this.selected.workDate.getMonth() + 1, day: this.selected.workDate.getDate()}
      ],
      openTime: [
        this.selected.openTime
      ],
      closeTime: [
        this.selected.closeTime
      ],
      weekday: [
        this.selected.weekday
      ],
      canUse: [
        this.selected.canUse
      ],
      isRange: [
        this.selected.isRange
      ],
      status: [
        this.selected.status
      ],
      isFrom: [
        this.selected.isFrom
      ],
      pointOfSaleId: [
        this.pointOfSaleService.selectedPointOfSale.id
      ]
    });

  }

  save() {

    console.log(this.selected);
    console.log(this.formGroup.valid);

    if (this.formGroup.valid) {

      const object = new WorkHoursModel().deserialize(this.formGroup.value, this.selected._isNew);
      object.id = this.selected.id;
      console.log(object);

      this.pointOfSaleService.saveWorkHours(object).then(() => {

        this.doFiltering();

      });

    }

  }

  delete() {

    if (confirm('Чи напевно хочеш видалити?') === true) {

      this.pointOfSaleService.deleteWorkHours(this.selected.id).then(() => {

        this.doFiltering();

      });

    }

  }
}
