import {Component, EventEmitter, forwardRef, Inject, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {PointOfSaleService} from '../../../shared/services/point-of-sale.service';
import {PointOfSaleModel} from '../../../shared/models/point-of-sale.model';
import {isNullOrUndefined, isNumber, isObject} from 'util';
import {AppService} from '../../../../../../../shared/services/app.service';
import {CityModel} from '../../../../../../../shared/models/city.model';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {environment} from '../../../../../../../../environments/environment';
import {CompanyService} from '../../../../admin-panel-company/shared/services/company.service';
import {CompanyModel} from '../../../../admin-panel-company/shared/models/company.model';

@Component({
  selector: 'app-admin-panel-point-of-sale-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnDestroy {

  id = 'point-of-sale-form';

  findByNameOrAddress: string;
  initialize = false;

  public dataAfterFind: Object;

  public loadingCompanies = false;
  public companies: CompanyModel[];

  @Input() submitted = false;

  constructor(
    private modalService: NgbModal,
    public router: Router,
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public pointOfSaleService: PointOfSaleService,
    private activatedRoute: ActivatedRoute,
    private companyService: CompanyService,
    public appService: AppService
  ) {

    this.dataAfterFind = null;

    if (this.model === undefined) {

      this.model = this.pointOfSaleService.selectedPointOfSale;

      if (isNullOrUndefined(this.model)) {

        this.model = new PointOfSaleModel();

      }

    } else {

      this.model.initForm();

    }

  }

  get cities(): CityModel[] {

    return this.appService.cities;

  }

  get logo(): string {

    return environment.host + 'uploads/companies/' + this.model.companyId + '/logo.jpg';

  }

  @Input()
  model: PointOfSaleModel;

  @Input()
  index: number;

  @Input()
  isFromCompanyForm = false;

  @Output()
  removeItem = new EventEmitter<number>();

  removeItemFromList() {
    this.removeItem.emit(this.index);
  }

  ngOnDestroy(): void {

    this.pointOfSaleService.selectedPointOfSale = null;

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      if (isNullOrUndefined(params.companyId)) {

        await this.companyService.getCompanies(true).then((result) => {

          this.companies = result;

          if (this.model._isNew) {

            this.initEventOnNgSelect();

          }

        });

      } else {


        this.model.companyId = Number(params.companyId);

        await this.companyService.getCompany(params.companyId).then((result) => {

          this.model.company = result;

        });
      }

      if (!isNullOrUndefined(params.id)) {

        await this.pointOfSaleService.getPointOfSale(params.id).then((result) => {

          this.model = result;
          this.model.initForm();

        });

      }

      this.model.initForm();
      this.initialize = true;

    });

  }

  private initEventOnNgSelect() {

    setTimeout(() => {
      document.getElementById('company-' + this.id).querySelector('input').addEventListener('input', function (evt) {
        console.log(evt.target.value);
        if (evt.target.value.length > 1) {

          const finedIndex = this.companies.findIndex((c) => c.name.toLocaleLowerCase().search(evt.target.value.toLowerCase()) > -1);

          if (finedIndex === -1) {

            this.loadingCompanies = true;
            this.companyService.filter.search = evt.target.value;

            this.companyService.getCompanies(true).then((documentList) => {

              this.companies = documentList;
              this.loadingCompanies = false;

            });

          }

        }
      }.bind(this));


    }, 250);

  }

  // Donbt touch this code!!!
  // private async sendArray(array: any[]) {
  //
  //   array = await array.filter((obj) => obj != null);
  //   console.log(array);
  //   await this.pointOfSaleService.postMigrateOldNoSqlDbToNew(array).then((a) => {
  //
  //     console.log(a);
  //
  //   });
  //
  // }

  changed($event) {
    console.log($event);
    console.log(this.model);
    this.findByNameOrAddress = ' ,' + $event.name;
  }

  savePOS() {

    this.submitted = true;

    if (this.model.validForm) {

      this.submitted = false;

      this.pointOfSaleService.savePointOfSale(this.model).then((result) => {

        if (isNumber(result) && result > 0) {

          this.model = new PointOfSaleModel();

          this.router.navigateByUrl('admin/point-of-sale/view/' + result);

        }

      });

    }
  }


  findLongAndLat() {

    //  https://api.opencagedata.com/geocode/v1/json?q=%D0%9F%D0%B0%D0%BD%D1%8C%D1%81%D0%BA%D0%B0%20%D0%B3%D1%83%D1%80%D0%B0%D0%BB%D0%BD%D1%8F,%D0%A7%D0%B5%D1%80%D0%BD%D1%96%D0%B2%D1%86%D1%96&key=e24b9801c37a491aba02b371cc2cac4b

    this.appService.authService.apiService.http.get('https://api.opencagedata.com/geocode/v1/json?q=' + this.findByNameOrAddress + '&language=uk&key=e24b9801c37a491aba02b371cc2cac4b').subscribe((result) => {

      console.log(result);
      this.dataAfterFind = result;

    });

  }

  useSelectData(data: any) {

    // this.model.formGroup.get('languages').controls.get('address').setValue(data['geometry']['formatted']);
    this.model.formGroup.get('latitude').setValue(data['geometry']['lat']);
    this.model.formGroup.get('longitude').setValue(data['geometry']['lng']);

  }

}
