import {LanguageInterface} from './interfaces/languageInterface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {FormBuilder, FormGroup} from '@angular/forms';

export class LanguageModel implements LanguageInterface, DeserializableInterface<LanguageInterface>, SerializableInterface<LanguageInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  address: string;
  id: number;
  languageCode: string;
  pointOfSaleId: number;

  _isNew: boolean;

  constructor() {

    this.initModel();
    this.initForm();

  }

  deserialize(input: LanguageInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): LanguageInterface {

    const object = Object.assign({} as LanguageInterface, this);
    delete object._isNew;
    delete object.formBuilder;

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {

      // this.latitude = this.formGroup.get('latitude').value;

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    this.formBuilder = new FormBuilder().group({
    });

  }

  public initModel() {

    this._isNew = true;

  }

}
