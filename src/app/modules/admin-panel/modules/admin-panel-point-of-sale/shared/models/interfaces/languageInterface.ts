export interface LanguageInterface {

  id: number;
  pointOfSaleId: number;
  languageCode: string;
  address: string;

}
