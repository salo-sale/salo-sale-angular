import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {isNullOrUndefined} from "util";
import {WorkHoursInterface} from './interfaces/work-hours.interface';

export class WorkHoursModel implements WorkHoursInterface, DeserializableInterface<WorkHoursInterface>, SerializableInterface<WorkHoursInterface> {

  canUse: number;
  closeTime: number;
  workDate: Date;
  id: number;
  isFrom: number;
  isRange: number;
  pointOfSaleId: number;
  openTime: number;
  weekday: number;
  status: number;

  _isNew: boolean;

  constructor() {
    this.initModel();
  }


  deserialize(input: WorkHoursInterface, isNew = false): this {

    console.log(input);
    this._isNew = isNew;
    const object = Object.assign(this, input);

    if (isNullOrUndefined(object.isFrom)) {
      object.isFrom = 0;
    }

    if (isNullOrUndefined(object.isRange)) {
      object.isRange = 0;
    }

    if (isNullOrUndefined(object.canUse)) {
      object.canUse = 1;
    }

    if (object.canUse) {
      object.canUse = 1;
    }


    if (!isNullOrUndefined(object.workDate)) {

      if (typeof object.workDate === 'string') {

        object.workDate = new Date(object.workDate);

      } else {

        console.log(object.workDate['month']);
        console.log(typeof object.workDate === 'string');
        object.workDate = new Date((object.workDate['year'] + '-' + object.workDate['month'] + '-' + object.workDate['day']));
      }

    }
    return object;

  }

  serialize(): WorkHoursInterface {

    const object = Object.assign({} as WorkHoursInterface, this);
    console.log(object);
    delete object._isNew;

    if (isNullOrUndefined(object.isFrom)) {
      object.isFrom = 0;
    }

    if (isNullOrUndefined(object.isRange)) {
      object.isRange = 0;
    }

    if (isNullOrUndefined(object.canUse)) {
      object.canUse = 1;
    }

    if (object.canUse) {
      object.canUse = 1;
    }

    if (!isNullOrUndefined(object.workDate)) {

      // object.validityDate = {
      //   year: object.validityDate.getFullYear(),
      //   month: object.validityDate.getMonth() + 1,
      //   day: object.validityDate.getDate()
      // } as Date | {year: number, month: number, day: number} & Date;

    }

    return object;

  }

  public initModel() {

    this._isNew = true;
    this.canUse = 1;
    this.status = 1;

  }

}
