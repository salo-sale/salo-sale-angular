import {PointOfSaleInterface} from './interfaces/point-of-sale.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {WorkHoursModel} from './work-hours.model';
import {LanguageModel} from './language.model';
import {LanguageInterface} from './interfaces/languageInterface';
import {CompanyModel} from '../../../admin-panel-company/shared/models/company.model';
import {isNullOrUndefined} from 'util';

export class PointOfSaleModel implements PointOfSaleInterface, DeserializableInterface<PointOfSaleInterface>, SerializableInterface<PointOfSaleInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  _isNew: boolean;

  localityId: number;
  comment: string;
  companyName: string;
  companyId: number;
  ikoint: number;
  id: number;
  latitude: number;
  longitude: number;
  status: number;

  company: CompanyModel;

  languages: LanguageModel[];
  workHours: WorkHoursModel[];

  constructor() {

    this.initModel();
    this.initForm();

  }

  deserialize(input: PointOfSaleInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): PointOfSaleInterface {

    const object = Object.assign({} as PointOfSaleInterface, this);
    delete object._isNew;
    delete object.formBuilder;
    delete object.company;
    object.companyId = isNullOrUndefined(object.companyId) ? (isNullOrUndefined(this.company) ? null : this.company.id) : object.companyId;
    object.languages = object.languages.map((language: LanguageModel) => language.serialize()) as LanguageInterface[] & LanguageModel[];

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {


    if (this.formGroup.valid) {

      const value = this.formGroup.value;

      this.status = value.status;
      this.latitude = value.latitude;
      this.company = value.company;
      this.longitude = value.longitude;
      this.comment = value.comment;
      this.localityId = value.localityId;
      this.languages = (value.languages as []).map(language => new LanguageModel().deserialize(language));

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    let languages = [
      new FormGroup({

        languageCode: new FormControl('uk', [Validators.required]),
        address: new FormControl('', [Validators.required]),

      })
    ];

    if (!this._isNew) {

      languages = [...this.languages.map((language) => {

        return new FormGroup({

          id: new FormControl(language.id, [Validators.required]),
          languageCode: new FormControl(language.languageCode, [Validators.required]),
          address: new FormControl(language.address, [Validators.required]),

        });

      })];

    }

    this.formBuilder = new FormBuilder().group({
      localityId: [
        this.localityId,
      ],
      status: [
        this.status,
      ],
      comment: [
        this.comment,
      ],
      company: [
        this.company,
        this._isNew ? [Validators.required] : []
      ],
      latitude: [
        this.latitude,
        [
          Validators.required
        ]
      ],
      longitude: [
        this.longitude,
        [
          Validators.required
        ]
      ],
      languages: new FormArray(languages, [Validators.required]),
    });

  }

  public initModel() {

    this._isNew = true;
    this.status = 1;
    this.languages = [];
    this.workHours = [];

  }

  get formLanguages(): FormArray {

    return this.formGroup.get('languages') as FormArray;

  }

  addBewLanguageToForm() {

    this.formLanguages.push(new FormGroup({

      languageCode: new FormControl('uk', [Validators.required]),
      address: new FormControl('', [Validators.required]),

    }));

  }

  deleteFormLanguage(index: number) {

    this.formLanguages.controls.splice(index, 1);

  }

}
