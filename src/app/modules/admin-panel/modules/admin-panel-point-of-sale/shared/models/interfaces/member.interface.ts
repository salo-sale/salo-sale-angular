export interface MemberInterface {

  id: number;
  pointOfSaleId: number;
  userId: number;
  role: number;
  status: number;
  createdAt: string;
  updatedAt: string;
  userEmail: string;

}
