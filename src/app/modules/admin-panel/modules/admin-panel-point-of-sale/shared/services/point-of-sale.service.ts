import { Injectable } from '@angular/core';
import {AppService} from '../../../../../../shared/services/app.service';
import {RequestConfigurationModel} from '../../../../../../shared/models/request-configuration.model';
import {PointOfSaleModel} from '../models/point-of-sale.model';
import {PointOfSaleInterface} from '../models/interfaces/point-of-sale.interface';
import {isNull} from 'util';
import {FilterModel} from '../models/filter.model';
import {NotificationStyle} from '../../../../shared/modules/notification/shared/enums/notification-style.enum';
import {NotificationService} from '../../../../shared/modules/notification/shared/services/notification.service';
import {ContactModel} from '../../../admin-panel-company/shared/models/contact.model';
import {FilterContactModel} from '../../../admin-panel-company/shared/models/filter-contact.model';
import {ContactInterface} from '../../../admin-panel-company/shared/models/interfaces/contact.interface';
import {FilterMemberModel} from '../models/filter-member.model';
import {MemberModel} from '../models/member.model';
import {MemberInterface} from '../models/interfaces/member.interface';
import {WorkHoursInterface} from '../models/interfaces/work-hours.interface';
import {FilterWorkHoursModel} from '../models/filter-work-hours.model';
import {WorkHoursModel} from '../models/work-hours.model';

@Injectable({
  providedIn: 'root'
})
export class PointOfSaleService {

  public filter: FilterModel;
  public filterMember: FilterMemberModel;
  public pointsOfSale: PointOfSaleModel[];
  public members: MemberModel[];
  private initialize: boolean;
  selectedPointOfSale: PointOfSaleModel;
  filterWorkHours: FilterWorkHoursModel;
  public workHoursList: WorkHoursModel[];

  contacts: ContactModel[];
  filterContact: FilterContactModel;

  constructor(
    public appService: AppService,
    public notificationService: NotificationService
  ) {

    this.init();

  }

  /**
   *
   * @param reset
   */
  async getWorkHours(reset: boolean = false): Promise<WorkHoursModel[]> {

    return new Promise<WorkHoursModel[]>(async (resolve, reject) => {

      if (this.workHoursList.length === 0 || reset) {

        this.filterWorkHours.pointOfSaleId = this.selectedPointOfSale.id;

        const filter = this.filterWorkHours.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'point-of-sale-work-hours',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: WorkHoursInterface[],
          total: number
        }) => {

          this.workHoursList = [];

          this.workHoursList = data.models.map((offer) => new WorkHoursModel().deserialize(offer));
          this.filterWorkHours.total = data.total;
          console.log(this.workHoursList);

          resolve(this.workHoursList);

        });

      } else {

        resolve(this.workHoursList);

      }

    });

  }

  async saveWorkHours(workHours: WorkHoursModel): Promise<any> {

    return new Promise<any>(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'point-of-sale-work-hours',
        subUrl: workHours._isNew ? '' : '/' + workHours.id.toString(),
        params: {
          type: workHours._isNew ? 'post' : 'put',
          body: workHours.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Графік роботи',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(result);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Пропозиція',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async deleteWorkHours(id: number): Promise<boolean> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'point-of-sale-work-hours',
        subUrl: '/' + id.toString(),
        params: {
          type: 'delete'
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Графік',
          body: 'Успішно видалений.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });

  }

  async getPointsOfSale(reset: boolean = false, companyId: number = null, offerId: number = null): Promise<PointOfSaleModel[]> {

    return new Promise<PointOfSaleModel[]>(async (resolve, reject) => {

      if (this.pointsOfSale.length === 0 || reset) {

        const filter = this.filter.serialize();

        delete filter['total'];
        delete filter['pageList'];

        if (!isNull(companyId)) {

          filter.companyId = companyId;

        }

        if (!isNull(offerId)) {

          filter.offerId = offerId;

        }

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'points-of-sale',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: PointOfSaleInterface[],
          total: number
        }) => {

          this.pointsOfSale = [];

          this.pointsOfSale = data.models.map((company) => new PointOfSaleModel().deserialize(company));
          this.filter.total = data.total;

          // LocalStorageTool.setCompanies(this.pointsOfSale);
          // LocalStorageTool.setFilterCompany(this.filter);

          resolve(this.pointsOfSale);

        });

      } else {

        resolve(this.pointsOfSale);

      }

    });

  }

  init() {

    this.filter = new FilterModel();
    this.filterMember = new FilterMemberModel();
    this.filterWorkHours = new FilterWorkHoursModel();
    this.pointsOfSale = [];
    this.selectedPointOfSale = null;
    this.filterContact = new FilterContactModel();
    this.members = [];
    this.workHoursList = [];
    this.contacts = [];

    if (!this.initialize) {

      // this.pointsOfSale = LocalStorageTool.getCompanies();
      //
      // this.filter = LocalStorageTool.getFilterCompany();
      this.filter.initPageList();

      this.initialize = true;

    }

  }

  async getContacts(reset: boolean = false, companyId: number = null): Promise<ContactModel[]> {

    return new Promise<ContactModel[]>(async (resolve, reject) => {

      console.log(this.contacts);

      if (this.contacts.length === 0 || reset) {

        const filter = this.filterContact.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'points-of-sale',
          subUrl: '/' + companyId + '/contacts',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: ContactInterface[],
          total: number
        }) => {

          this.contacts = [];

          this.contacts = data.models.map((company) => new ContactModel().deserialize(company));
          this.filterContact.total = data.total;

          // LocalStorageTool.setCompanies(this.contacts);
          // LocalStorageTool.setFilterCompany(this.filter);

          resolve(this.contacts);

        });

      } else {

        resolve(this.contacts);

      }

    });

  }

  async deleteContact(id: number): Promise<boolean> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'points-of-sale',
        subUrl: '/' + this.selectedPointOfSale.id.toString() + '/contact/' + id.toString(),
        params: {
          type: 'delete'
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Контакт',
          body: 'Успішно видалений.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });

  }

  async saveContact(model: ContactModel): Promise<boolean | number> {

    return new Promise(async (resolve) => {

      console.log(model);

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'points-of-sale',
        subUrl: '/' + model.companyId.toString() + '/contact' + (model._isNew ? '' : '/' + model.id.toString()),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Контакт',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  /**
   *
   * @param model
   */
  async savePointOfSale(model: PointOfSaleModel): Promise<number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'points-of-sale',
        subUrl: model._isNew ? '' : '/' + model.id,
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Точка продажу',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Точка продажу',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  /**
   *
   * @param id
   */
  async getPointOfSale(id: string): Promise<PointOfSaleModel> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'points-of-sale',
        subUrl: '/' + id
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((company: PointOfSaleInterface) => {

        console.log(company);
        this.selectedPointOfSale = new PointOfSaleModel().deserialize(company);

        resolve(this.selectedPointOfSale);

      });

    });

  }

  /**
   *
   * @param data
   */
  // async postMigrateOldNoSqlDbToNew(data: MigrateCompanyInterface[]): Promise<boolean> {
  //
  //   return new Promise(async (resolve) => {
  //
  //     const requestConfiguration = new RequestConfigurationModel().deserialize({
  //       path: 'admin',
  //       url: 'migrate-old-db',
  //       params: {
  //         type: 'post',
  //         body: data
  //       }
  //     });
  //
  //     await this.appService.authService.apiService.request(requestConfiguration).subscribe((res) => {
  //
  //       console.log(res);
  //
  //       resolve(true);
  //
  //     });
  //
  //   });
  //
  // }

  /**
   *
   * @param offerId
   * @param pos
   * @param remove
   */
  async togglePosToOffer(offerId: number, pos: PointOfSaleModel, remove = false): Promise<number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'offer',
        subUrl: offerId.toString() + '/toggle-point-of-sale-to-offer/' + pos.id.toString(),
        params: {
          type: remove ? 'delete' : 'post',
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Точка продажу',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Точка продажу',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

  async getMembers(reset: boolean = false, pointOfSaleId: number = null): Promise<MemberModel[]> {

    return new Promise<MemberModel[]>(async (resolve, reject) => {

      console.log(this.contacts);

      if (this.contacts.length === 0 || reset) {

        const filter = this.filterMember.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'points-of-sale',
          subUrl: '/' + pointOfSaleId + '/members',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: MemberModel[],
          total: number
        }) => {

          this.members = [];

          if (data.total > 0) {
            this.members = data.models.map((company) => new MemberModel().initModel().deserialize(company));
          }

          this.filterMember.total = data.total;

          resolve(this.members);

        });

      } else {

        resolve(this.members);

      }

    });

  }

  async deleteMember(id: number): Promise<boolean> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'points-of-sale',
        subUrl: '/' + this.selectedPointOfSale.id.toString() + '/member/' + id.toString(),
        params: {
          type: 'delete'
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((result) => {

        console.log(result);

        this.notificationService.addToStream({
          title: 'Контакт',
          body: 'Успішно видалений.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(true);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(false);

      });

    });

  }

  async saveMember(model: MemberModel): Promise<number> {

    return new Promise(async (resolve) => {

      console.log(model);

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'points-of-sale',
        subUrl: '/' + model.pointOfSaleId.toString() + '/member' + (model._isNew ? '' : '/' + model.id.toString()),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((success: number) => {


        if (success > 0) {

          this.notificationService.addToStream({
            title: 'Член',
            body: 'Успішно записано.',
            show: true,
            style: NotificationStyle.SUCCESS
          });

        } else {

          this.notificationService.addToStream({
            title: 'Член',
            body: 'Помилка.',
            show: true,
            style: NotificationStyle.ERROR
          });

        }

        resolve(success);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контакт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

}
