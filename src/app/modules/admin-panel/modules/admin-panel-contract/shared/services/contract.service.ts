import { Injectable } from '@angular/core';
import {AppService} from '../../../../../../shared/services/app.service';
import {NotificationService} from '../../../../shared/modules/notification/shared/services/notification.service';
import {ApiService} from '../../../../../../shared/services/api.service';
import {RequestConfigurationModel} from '../../../../../../shared/models/request-configuration.model';
import {ContractModel} from '../models/contract.model';
import {FilterContractModel} from '../models/filter-contract.model';
import {ContractInterface} from '../models/interfaces/contract.interface';
import {isNull} from 'util';
import {CompanyModel} from '../../../admin-panel-company/shared/models/company.model';
import {NotificationStyle} from '../../../../shared/modules/notification/shared/enums/notification-style.enum';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  public filter: FilterContractModel;
  private initialize: boolean;
  public contracts: ContractModel[];
  selectedModel: ContractModel;

  constructor(
    public appService: AppService,
    public apiService: ApiService,
    public notificationService: NotificationService
  ) {

    this.filter = new FilterContractModel();
    this.selectedModel = null;

    this.contracts = [];

  }

  init() {

    if (!this.initialize) {

      this.initialize = true;

      // this.getContracts();

    }

  }

  async getContracts(reset: boolean = false, companyId: number = null): Promise<ContractModel[]> {

    return new Promise<ContractModel[]>(async (resolve, reject) => {

      if (this.contracts.length === 0 || reset) {

        const filter = this.filter.serialize();

        delete filter['total'];
        delete filter['pageList'];

        if (!isNull(companyId)) {

          filter.companyId = companyId;

        }

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'contracts',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: ContractInterface[],
          total: number
        }) => {

          this.contracts = [];

          this.contracts = data.models.map((contract) => new ContractModel().deserialize(contract));
          this.filter.total = data.total;

          // LocalStorageTool.setCompanies(this.contracts);
          // LocalStorageTool.setFilterCompany(this.filter);

          resolve(this.contracts);

        });

      } else {

        resolve(this.contracts);

      }

    });

  }


  async save(model: ContractModel): Promise<boolean | number> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'contracts',
        subUrl: model._isNew ? '' : '/' + model.id.toString(),
        params: {
          type: model._isNew ? 'post' : 'put',
          body: model.serialize()
        }
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((id: number) => {

        console.log(id);

        this.notificationService.addToStream({
          title: 'Контракт',
          body: 'Успішно записано.',
          show: true,
          style: NotificationStyle.SUCCESS
        });

        resolve(id);

      }, error => {

        console.log(error);


        // this.notificationService.addToStream({
        //   title: 'Контракт',
        //   body: error.error,
        //   show: true,
        //   style: NotificationStyle.ERROR
        // });

        resolve(0);

      });

    });

  }

}
