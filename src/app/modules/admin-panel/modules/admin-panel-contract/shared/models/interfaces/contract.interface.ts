export interface ContractInterface {

  id: number;
  generatedNumber: string;
  companyId: number;
  validityFrom: string;
  validityTo: string;
  status: number;
  typeValidity: number;

}
