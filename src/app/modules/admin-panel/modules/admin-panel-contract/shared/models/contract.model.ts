import {ContractInterface} from './interfaces/contract.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';
import {isNullOrUndefined} from "util";

export class ContractModel implements ContractInterface, DeserializableInterface<ContractInterface>, SerializableInterface<ContractInterface> {

  /**
   *
   * Declaration variables
   *
   */

  private formBuilder: FormGroup;

  id: number;
  generatedNumber: string;
  companyId: number;
  validityFrom: string;
  validityTo: string;
  typeValidity: number; // 0 - Day 1 - Days 2 - Month 3 - Months 4 - Year 5 - Years
  status: number;

  // TODO Added pointsOfSale

  _isNew: boolean;

  constructor() {

    this._isNew = true;
    this.typeValidity = 2;
    this.initForm();

  }

  deserialize(input: ContractInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): ContractInterface {

    const object = Object.assign({} as ContractInterface, this);
    delete object._isNew;
    delete object.formBuilder;

    return object;

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  get validForm(): boolean {

    if (this.formGroup.valid) {


      Object.keys(this.formGroup.value).forEach(key => {

        if (!isNullOrUndefined(this.formGroup.value[key])) {

          this[key] = this.formGroup.value[key];

        }

      });

      return true;

    } else {

      return false;

    }

  }

  public initForm(): void {

    this.formBuilder = new FormBuilder().group({
      typeValidity: [
        this.typeValidity,
      ],
      validityFrom: [
        this.validityFrom,
      ],
      validityTo: [
        this.validityTo,
      ],
    });

  }

}
