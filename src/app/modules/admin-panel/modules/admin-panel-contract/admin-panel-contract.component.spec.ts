import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPanelContractComponent } from './admin-panel-contract.component';

describe('AdminPanelCompanyComponent', () => {
  let component: AdminPanelContractComponent;
  let fixture: ComponentFixture<AdminPanelContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPanelContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPanelContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
