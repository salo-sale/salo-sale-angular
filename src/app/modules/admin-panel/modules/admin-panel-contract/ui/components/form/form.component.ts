import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '../../../../../../../shared/services/app.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ContractService} from '../../../shared/services/contract.service';
import {ContractModel} from '../../../shared/models/contract.model';
import {isNullOrUndefined} from 'util';
import {OfferModel} from '../../../../admin-panel-offer/shared/models/offer.model';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-admin-panel-offer-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  public model: ContractModel;

  from: NgbDateStruct;
  to: NgbDateStruct;

  public submitted: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    public appService: AppService,
    public contractService: ContractService,
    private router: Router,
  ) {

    this.submitted = false;

    if (isNullOrUndefined(contractService.selectedModel)) {

      this.model = new ContractModel();

    } else {

      this.model = contractService.selectedModel;

    }

    this.model.initForm();

    const now = new Date();

    this.from = {
      year: now.getFullYear(),
      month: now.getMonth(),
      day: now.getDate()
    };

    if (this.model.typeValidity === 2) {

      now.setMonth(now.getMonth() + 1);

    }

    this.to = {
      year: now.getFullYear(),
      month: now.getMonth(),
      day: now.getDate()
    };

  }

  public tags: {id: number, name: string}[];

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {

      if (!isNullOrUndefined(params.companyId)) {

        this.model.companyId = Number(params.companyId);

      }

    });

  }

  changed(data) {

    console.log(data);

    // this.model.tags = data.value.map(value => parseInt(value));

  }

  save() {

    this.submitted = true;

    if (this.model.validForm) {

      this.contractService.save(this.model).then(result => {

        if (result) {

          this.router.navigateByUrl('admin/contract/view/' + result);

        }

      });

    } else {

      console.log(this.model.formGroup.errors);

    }

  }

}
