import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss']
})
export class ViewPageComponent implements OnInit {

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    private activatedRoute: ActivatedRoute,
  ) {

    adminService.titlePage = 'Перегляд пропозиції';

  }

  ngOnInit() {

    this.activatedRoute.parent.firstChild.params.subscribe(async (params) => {


    });

  }

}
