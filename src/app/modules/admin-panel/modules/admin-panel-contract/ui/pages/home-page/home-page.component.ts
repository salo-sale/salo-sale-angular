import {Component, forwardRef, Inject, Input, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../../../shared/services/admin-panel.service';
import {ContractModel} from '../../../shared/models/contract.model';
import {ContractService} from '../../../shared/services/contract.service';
import {isNullOrUndefined} from 'util';
import {FilterContractModel} from '../../../shared/models/filter-contract.model';

@Component({
  selector: 'app-admin-panel-contract-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  id = 'contracts';
  pageId = 'app-admin-contracts-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  public adminService: AdminPanelService;

  @Input()
  companyId: number = null;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public contractService: ContractService
  ) {

    this.adminService = adminService;

  }

  get filter(): FilterContractModel {

    return this.contractService.filter;

  }

  get contracts(): ContractModel[] {

    return this.contractService.contracts;

  }

  ngOnInit() {

    if (isNullOrUndefined(this.companyId)) {

      this.initHideBody = false;

      this.adminService.titlePage = 'Контракти';

    } else {

      this.initHideBody = true;

    }


    this.collapseContent = !this.initHideBody;

    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }


  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.contractService.filter.resetPage();

    }

    this.contractService.getContracts(true, this.companyId).then(() => {

      if (initPageList) {

        this.contractService.filter.initPageList();

      }

      this.loader(false);

    });

  }


  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    console.log(this.filter.total);

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

}
