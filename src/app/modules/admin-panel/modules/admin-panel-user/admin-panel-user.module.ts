import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './ui/pages/home-page/home-page.component';
import {AdminPanelUserRoutingModule} from './admin-panel-user-routing.module';
import {AdminPanelUserComponent} from './admin-panel-user.component';
import {FormsModule} from '@angular/forms';
import { ViewPageComponent } from './ui/pages/view-page/view-page.component';
import {KarbaCoreModule} from '../../shared/modules/karba-core/karba-core.module';

@NgModule({
    declarations: [AdminPanelUserComponent, HomePageComponent, ViewPageComponent],
    exports: [
        HomePageComponent
    ],
    imports: [
        CommonModule,
        AdminPanelUserRoutingModule,
        FormsModule,
        KarbaCoreModule
    ]
})
export class AdminPanelUserModule { }
