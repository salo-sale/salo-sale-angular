import { Injectable } from '@angular/core';
import {isNull} from "util";
import {RequestConfigurationModel} from '../../../../../../shared/models/request-configuration.model';
import {PointOfSaleInterface} from '../../../admin-panel-point-of-sale/shared/models/interfaces/point-of-sale.interface';
import {UserModel} from '../models/user.model';
import {FilterModel} from '../models/filter.model';
import {AppService} from '../../../../../../shared/services/app.service';
import {UserInterface} from '../models/interfaces/user.interface';
import {PointOfSaleModel} from '../../../admin-panel-point-of-sale/shared/models/point-of-sale.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public users: UserModel[];
  public user: UserModel;
  public filter: FilterModel;

  constructor(
    public appService: AppService,
  ) {

    this.init();

  }

  async getUsers(reset: boolean = false): Promise<UserModel[]> {

    return new Promise<UserModel[]>(async (resolve, reject) => {

      if (this.users.length === 0 || reset) {

        const filter = this.filter.serialize();

        delete filter['total'];
        delete filter['pageList'];

        const requestConfiguration = new RequestConfigurationModel().deserialize({
          path: 'admin',
          url: 'users',
          params: {
            options: filter,
          }
        });

        await this.appService.authService.apiService.request(requestConfiguration).subscribe((data: {
          models: UserInterface[],
          total: number
        }) => {

          this.users = [];

          this.users = data.models.map((user) => new UserModel().deserialize(user));
          this.filter.total = data.total;

          // LocalStorageTool.setCompanies(this.users);
          // LocalStorageTool.setFilterCompany(this.filter);

          resolve(this.users);

        });

      } else {

        resolve(this.users);

      }

    });

  }

  /**
   *
   * @param id
   */
  async getUser(id: string): Promise<UserModel> {

    return new Promise(async (resolve) => {

      const requestConfiguration = new RequestConfigurationModel().deserialize({
        path: 'admin',
        url: 'user',
        subUrl: id
      });

      await this.appService.authService.apiService.request(requestConfiguration).subscribe((company: UserInterface) => {

        console.log(company);
        this.user = new UserModel().deserialize(company);

        resolve(this.user);

      });

    });

  }

  init() {

    this.user = new UserModel();
    this.users = [];
    this.filter = new FilterModel();

  }
}
