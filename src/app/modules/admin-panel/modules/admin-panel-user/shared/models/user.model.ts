import {UserInterface} from './interfaces/user.interface';
import {DeserializableInterface} from '../../../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../../../shared/models/interfaces/serializable.interface';

export class UserModel implements UserInterface, DeserializableInterface<UserInterface>, SerializableInterface<UserInterface> {

  /**
   *
   * Declaration variables
   *
   */

  _isNew: boolean;

  birthday: string;
  createdAt: string;
  email: string;
  firstName: string;
  gender: number;
  id: number;
  ikoint: number;
  invitationCodes: string[];
  invitationUserId: number;
  lastName: string;
  phone: string;
  roles: string[];
  status: number;
  updatedAt: string;

  deserialize(input: UserInterface): this {

    this._isNew = false;

    return Object.assign(this, input);

  }

  serialize(): UserInterface {

    const object = Object.assign({} as UserInterface, this);
    delete object._isNew;

    return object;

  }

  initModel() {

    this._isNew = true;

  }

}
