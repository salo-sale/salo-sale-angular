export interface FilterInterface {

  page: number;
  count: number;
  orderBy: string;
  sort: string;
  status: number;
  companyId: number;
  search: string;

}
