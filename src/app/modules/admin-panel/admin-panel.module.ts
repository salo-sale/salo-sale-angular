import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminPanelRoutingModule} from './admin-panel-routing.module';
import { HomePageComponent } from './ui/pages/home-page/home-page.component';
import {AdminPanelComponent} from './admin-panel.component';
import {AdminPanelService} from './shared/services/admin-panel.service';
import { HeaderComponent } from './ui/components/header/header.component';
import {NgbModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import { SidebarComponent } from './ui/components/sidebar/sidebar.component';
import { LogoutModalComponent } from './ui/components/logout-modal/logout-modal.component';
import { FloatingButtonScrollToTopComponent } from './ui/components/floating-button-scroll-to-top/floating-button-scroll-to-top.component';
import { FooterComponent } from './ui/components/footer/footer.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TagsPageComponent } from './ui/pages/tags-page/tags-page.component';
import { CitiesPageComponent } from './ui/pages/cities-page/cities-page.component';
import * as $ from 'jquery';
import {isNullOrUndefined} from 'util';
import {NgSelectModule} from '@ng-select/ng-select';
import {SlideCheckboxComponent} from './ui/components/slide-checkbox/slide-checkbox.component';
import {KarbaCoreModule} from './shared/modules/karba-core/karba-core.module';
import { TagsComponent } from './ui/components/tags/tags.component';
import { TagsListComponent } from './ui/components/tags-list/tags-list.component';

@NgModule({
    declarations: [
      AdminPanelComponent,
      HomePageComponent,
      HeaderComponent,
      SidebarComponent,
      LogoutModalComponent,
      FloatingButtonScrollToTopComponent,
      FooterComponent,
      TagsPageComponent,
      CitiesPageComponent,
      SlideCheckboxComponent,
      TagsComponent,
      TagsListComponent
    ],
    providers: [
        AdminPanelService
    ],
    exports: [
        CitiesPageComponent
    ],
    imports: [
        CommonModule,
        AdminPanelRoutingModule,
        NgbModule,
        NgbTooltipModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        KarbaCoreModule
    ]
})
export class AdminPanelModule {

  constructor(
    private adminService: AdminPanelService
  ) {

    adminService.init();

  }

}
