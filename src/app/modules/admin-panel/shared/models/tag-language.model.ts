import {TagLanguageInterface} from './interfaces/tag-language.interface';
import {DeserializableInterface} from '../../../../shared/models/interfaces/deserializable.interface';
import {SerializableInterface} from '../../../../shared/models/interfaces/serializable.interface';

export class TagLanguageModel implements TagLanguageInterface, DeserializableInterface<TagLanguageInterface>, SerializableInterface<TagLanguageInterface> {

  languageCode: string;
  name: string;
  id: number;

  deserialize(input: TagLanguageInterface): this {

    return Object.assign(this, input);

  }

  serialize(): TagLanguageInterface {

    return Object.assign({} as TagLanguageInterface, this);

  }

}
