export interface CityLanguageInterface {

  id: number;
  localityId: number;
  languageCode: string;
  name: string;

}
