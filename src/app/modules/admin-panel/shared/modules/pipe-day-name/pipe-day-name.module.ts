import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PipeDayNamePipe} from './shared/pipes/pipe-day-name.pipe';

@NgModule({
  declarations: [
    PipeDayNamePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PipeDayNamePipe
  ],
})
export class PipeDayNameModule { }
