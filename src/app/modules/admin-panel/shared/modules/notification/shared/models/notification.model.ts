import {NotificationInterface} from './interfaces/notification.interface';
import {NotificationStyle} from '../enums/notification-style.enum';
import {DeserializableInterface} from '../../../../../../../shared/models/interfaces/deserializable.interface';

export class NotificationModel implements NotificationInterface, DeserializableInterface<NotificationInterface> {

  _bgColor: string;

  body: string;
  link?: string;
  show: boolean;
  showCloseButton?: boolean;
  timer?: number;
  title: string;
  style: NotificationStyle;
  useTimer?: boolean;

  constructor(notification: NotificationInterface = null) {

    this.body = 'Body';
    this.link = null;
    this.show = false;
    this.showCloseButton = true;
    this.timer = 3000;
    this.title = 'Title';
    this.style = NotificationStyle.PRIMARY;
    this.useTimer = true;

    this.initBgColor();
    this.initTimer();

  }

  /**
   *
   * @param input
   * @return NotificationInterface
   */
  deserialize(input: NotificationInterface): this {

    const object = Object.assign(this, input);
    this.initBgColor();
    this.initTimer();
    return object;

  }

  initBgColor() {

    switch (this.style) {
      case NotificationStyle.PRIMARY:
        this._bgColor = 'bg-primary';
        break;
      case NotificationStyle.SUCCESS:
        this._bgColor = 'bg-success';
        break;
      case NotificationStyle.ERROR:
        this._bgColor = 'bg-danger';
        break;
      case NotificationStyle.INFO:
        this._bgColor = 'bg-info';
        break;
      case NotificationStyle.WARNING:
        this._bgColor = 'bg-warning';
        break;

    }

  }

  initTimer() {

    if (this.show && this.useTimer) {

      setTimeout(() => {

        this.show = false;

      }, this.timer);

    }

  }

}
