import {Component, forwardRef, Inject, Input, OnInit} from '@angular/core';
import {OfferLocalityInterface} from '../../../../../../modules/admin-panel-offer/shared/models/interfaces/offer-locality.interface';
import {LocalityAdminModel} from '../../../../../models/locality-admin.model';
import {AdminPanelService} from '../../../../../services/admin-panel.service';
import {AppService} from '../../../../../../../../shared/services/app.service';
import {CityStatusEnum} from '../../../../../../../../shared/enums/status/city.status.enum';
import {isNullOrUndefined} from 'util';
import {OfferService} from '../../../../../../modules/admin-panel-offer/shared/services/offer.service';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-locality-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss']
})
export class ListPageComponent implements OnInit {
  id = 'locality-global-component';
  pageId = 'app-admin-locality-global-component-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;

  public adminService: AdminPanelService;
  public selectedItem: LocalityAdminModel;

  @Input()
  offerLocalityList: OfferLocalityInterface[] = null;

  @Input()
  offerId: number = null;

  constructor(
    public offerService: OfferService,
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public appService: AppService,
    private deviceService: DeviceDetectorService
  ) {

    this.isDesktop = deviceService.isDesktop();
    this.adminService = adminService;
    this.selectedItem = new LocalityAdminModel();

  }

  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    if (this.adminService.filterCity.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  get localities(): LocalityAdminModel[] {

    return this.adminService.cities;

  }

  ngOnInit() {

    if (this.offerId === null) {

      this.adminService.titlePage = 'Міста';

      this.initHideBody = false;

    } else {

      this.initHideBody = true;

    }

    this.collapseContent = !this.initHideBody;


    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);


    if (initPageList) {

      this.adminService.filterCity.resetPage();

    }

    this.adminService.getCities(true).then(() => {

      if (initPageList) {

        this.adminService.filterCity.initPageList();

      }

      this.loader(false);

    });

  }

  initSelectedItem(selectedItem = new LocalityAdminModel(), isCreate: boolean = false) {

    if (isCreate) {

      console.log(selectedItem);

      this.selectedItem = new LocalityAdminModel();

      if (selectedItem.type === 'COUNTRY') {

        this.selectedItem.countryId = selectedItem.id;
        this.selectedItem.type = 'REGION';

      } else if (selectedItem.type === 'REGION') {

        this.selectedItem.countryId = selectedItem.countryId;
        this.selectedItem.regionId = selectedItem.id;
        this.selectedItem.type = 'CITY';

      } else if (selectedItem.type === 'CITY') {

        this.selectedItem.countryId = selectedItem.countryId;
        this.selectedItem.regionId = selectedItem.regionId;
        this.selectedItem.cityId = selectedItem.id;
        this.selectedItem.type = 'LOCALITY';

      }

    } else {

      this.selectedItem = selectedItem;

    }

    this.selectedItem.initForm();
    console.log(this.selectedItem.formGroup);

  }

  saveSelectedLocality() {
    if (this.selectedItem.validForm) {

      console.log(this.selectedItem.serialize());

      this.adminService.saveLocality(this.selectedItem).then((result) => {

        if (result > 0) {

          this.appService.getCities(true);
          document.getElementById('app-admin-panel-cities-cancel-button-modal').click();

        }

      });

    }
  }

  deleteSelectedLocality() {

    console.log(this.selectedItem.serialize());

    this.adminService.deleteLocality(this.selectedItem).then((result) => {

      if (result > 0) {

        this.appService.getCities(true);
        document.getElementById('app-admin-panel-cities-cancel-button-modal').click();

      }

    });
  }

  statusName(status) {

    return CityStatusEnum[status];

  }

  checkLocalityInOfferLocalityList(localityId: number): OfferLocalityInterface {

    const locality = this.offerLocalityList.find((locality) => locality.localityId === localityId);
    return isNullOrUndefined(locality) ? null : locality;

  }

  useLocalityForOffer(localityId: number, use: boolean = true) {

    // TODO alert, Напевно хочеш заборонити?

    this.offerService.toggleOfferLocality({
      offerId: this.offerId,
      without: use ? 0 : 1,
      localityId: localityId
    }).then(async () => {
      await this.offerService.getOffer(this.offerId.toString());
    });


  }

  deleteLocalityForOffer(localityId: number) {
    this.offerService.toggleOfferLocality({
      id: this.checkLocalityInOfferLocalityList(localityId).id,
      offerId: this.offerId,
      without: 0,
      localityId: localityId
    }, true).then(async () => {
      await this.offerService.getOffer(this.offerId.toString());
    });
  }
}
