import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KarbaTitleComponent } from './ui/components/karba-title/karba-title.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [KarbaTitleComponent],
  exports: [
    KarbaTitleComponent
  ],
    imports: [
        CommonModule,
        RouterModule
    ]
})
export class KarbaCoreModule { }
