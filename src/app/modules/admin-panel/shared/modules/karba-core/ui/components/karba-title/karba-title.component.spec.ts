import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KarbaTitleComponent } from './karba-title.component';

describe('KarbaTitleComponent', () => {
  let component: KarbaTitleComponent;
  let fixture: ComponentFixture<KarbaTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KarbaTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KarbaTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
