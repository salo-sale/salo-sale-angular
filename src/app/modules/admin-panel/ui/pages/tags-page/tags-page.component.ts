import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../shared/services/admin-panel.service';
import {TagModel} from '../../../shared/models/tag.model';
import {AppService} from '../../../../../shared/services/app.service';
import {ListOfTagsModel} from '../../../../../shared/models/list-of-tags.model';
import {CityStatusEnum} from '../../../../../shared/enums/status/city.status.enum';
import {TagStatusEnum} from '../../../../../shared/enums/status/tag.status.enum';

@Component({
  selector: 'app-tags-page',
  templateUrl: './tags-page.component.html',
  styleUrls: ['./tags-page.component.scss']
})
export class TagsPageComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
