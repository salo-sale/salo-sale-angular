import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../shared/services/admin-panel.service';
import {AppService} from '../../../../../shared/services/app.service';
import {LocalityAdminModel} from '../../../shared/models/locality-admin.model';
import {CityStatusEnum} from '../../../../../shared/enums/status/city.status.enum';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-cities-page',
  templateUrl: './cities-page.component.html',
  styleUrls: ['./cities-page.component.scss']
})
export class CitiesPageComponent implements OnInit {
  id = 'cities';
  pageId = 'app-admin-cities-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  isDesktop = false;
  public adminService: AdminPanelService;
  public selectedItem: LocalityAdminModel;
  submitted: boolean;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public appService: AppService,
    private deviceService: DeviceDetectorService
  ) {

    this.isDesktop = deviceService.isDesktop();
    this.submitted = false;
    adminService.titlePage = 'Міста';
    this.adminService = adminService;
    this.selectedItem = new LocalityAdminModel();

  }

  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  clearSearch() {

    this.adminService.filterCity.search = '';
    this.doFiltering();

  }

  get filter() {

    return this.adminService.filterCity;

  }

  get localities(): LocalityAdminModel[] {

    return this.adminService.cities;

  }

  ngOnInit() {

    this.initHideBody = false;
    this.collapseContent = !this.initHideBody;


    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);

    if (initPageList) {

      this.adminService.filterCity.resetPage();

    }

    this.adminService.getCities(true).then(() => {

      if (initPageList) {

        this.adminService.filterCity.initPageList();

      }

      this.loader(false);

    });

  }

  initSelectedItem(selectedItem = new LocalityAdminModel(), isCreate: boolean = false) {

    if (isCreate) {

      console.log(selectedItem);

      this.selectedItem = new LocalityAdminModel();

      if (selectedItem.type === 'COUNTRY') {

        this.selectedItem.countryId = selectedItem.id;
        this.selectedItem.type = 'REGION';

      } else if (selectedItem.type === 'REGION') {

        this.selectedItem.countryId = selectedItem.countryId;
        this.selectedItem.regionId = selectedItem.id;
        this.selectedItem.type = 'CITY';

      } else if (selectedItem.type === 'CITY') {

        this.selectedItem.countryId = selectedItem.countryId;
        this.selectedItem.regionId = selectedItem.regionId;
        this.selectedItem.cityId = selectedItem.id;
        this.selectedItem.type = 'LOCALITY';

      }

    } else {

      this.selectedItem = selectedItem;

    }

    this.selectedItem.initForm();
    console.log(this.selectedItem.formGroup);

  }

  saveSelectedLocality() {
    this.submitted = true;
    if (this.selectedItem.validForm) {

      console.log(this.selectedItem.serialize());

      this.adminService.saveLocality(this.selectedItem).then((result) => {

        if (result > 0) {

          this.appService.getCities(true);
          document.getElementById('app-admin-panel-cities-cancel-button-modal').click();
          this.submitted = false;

        }

      });

    }
  }

  deleteSelectedLocality() {

    console.log(this.selectedItem.serialize());

    this.adminService.deleteLocality(this.selectedItem).then((result) => {

      if (result > 0) {

        this.appService.getCities(true);
        document.getElementById('app-admin-panel-cities-cancel-button-modal').click();

      }

    });
  }

  getStatusName(status) {

    return CityStatusEnum[status];

  }

}
