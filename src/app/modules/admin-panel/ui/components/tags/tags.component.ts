import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AdminPanelService} from '../../../shared/services/admin-panel.service';
import {TagModel} from '../../../shared/models/tag.model';
import {ListOfTagsModel} from '../../../../../shared/models/list-of-tags.model';
import {AppService} from '../../../../../shared/services/app.service';
import {TagStatusEnum} from '../../../../../shared/enums/status/tag.status.enum';

@Component({
  selector: 'app-tags-component',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {
  id = 'tags-component';
  pageId = 'app-admin-tags-component-home';

  public initialized = false;
  public initHideBody = true;
  public collapseContent = true;
  public loading = true;

  public adminService: AdminPanelService;
  public selectedItem: TagModel;
  selectedListOfTags: ListOfTagsModel;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public appService: AppService,
  ) {

    adminService.titlePage = 'Теги';
    this.adminService = adminService;
    this.selectedItem = new TagModel();
    this.selectedListOfTags = null;

  }

  clickOnRefreshBtn() {

    this.doFiltering();

  }

  clickOnCollapseBtn() {

    this.collapseContent = !this.collapseContent;

    if (this.filter.total === 0 && this.collapseContent) {

      this.doFiltering();

    }

  }

  private init() {

    if (!this.initialized) {

      this.initialized = true;


    }

  }

  loader(run: boolean) {
    this.loading = run;
  }

  get tags(): TagModel[] {

    return this.adminService.tags;

  }

  clearSearch() {

    this.adminService.filterTag.search = '';
    this.doFiltering();

  }

  get filter() {

    return this.adminService.filterTag;

  }

  ngOnInit() {

    this.initHideBody = true;
    this.collapseContent = !this.initHideBody;


    this.init();

    if (this.initHideBody) {

      this.loader(false);

    } else {

      this.doFiltering();

    }

  }

  getStatusName(status) {

    return TagStatusEnum[status];

  }

  doFiltering(initPageList: boolean = true) {

    this.loader(true);


    if (initPageList) {

      this.adminService.filterTag.resetPage();

    }

    this.adminService.getTags(true).then(() => {

      if (initPageList) {

        this.adminService.filterTag.initPageList();

      }

      this.loader(false);

    });

  }

  initSelectedItem(selectedItem = new TagModel()) {

    this.selectedItem = selectedItem;
    this.selectedItem.initForm();
    console.log(this.selectedItem.formGroup);

  }

  saveSelectedTag() {
    if (this.selectedItem.validForm) {

      console.log(this.selectedItem.serialize());

      this.adminService.saveTags(this.selectedItem).then((result) => {

        if (result > 0) {

          document.getElementById('app-admin-panel-tags-cancel-button-modal').click();

        }

      });

    }
  }

  initSelectedListOfTags(listOfTags: ListOfTagsModel) {

    this.selectedListOfTags = listOfTags;

    console.log(listOfTags);

  }

  checkUseForList(id: number): boolean {

    return this.selectedListOfTags.tags.findIndex((tag) => tag.id === id) > -1;

  }

  toggleToTagList(tag: TagModel, isDelete = false) {

    this.adminService.toggleTagToTagList(tag, isDelete, this.selectedListOfTags.name).then((result) => {

      console.log(result);

      if (result) {

        localStorage.removeItem('listsOfTags');

        if (isDelete) {

          this.selectedListOfTags.tags.splice(this.selectedListOfTags.tags.findIndex(t => t.id === tag.id), 1);

        } else {

          this.selectedListOfTags.tags.push(tag);

        }

      }

    });

  }

}
