import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-panel-floating-button-scroll-to-top',
  templateUrl: './floating-button-scroll-to-top.component.html',
  styleUrls: ['./floating-button-scroll-to-top.component.scss']
})
export class FloatingButtonScrollToTopComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
