import {Component, forwardRef, Inject, OnDestroy, OnInit} from '@angular/core';
import {routes} from '../../../admin-panel-routing.module';
import {Routes} from '@angular/router';
import {AdminPanelService} from '../../../shared/services/admin-panel.service';
import * as $ from 'jquery';
import {Subscription} from 'rxjs';
import {AuthPanelService} from '../../../../auth-panel/shared/services/auth-panel.service';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-admin-panel-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {

  public routes: Routes;
  public adminService: AdminPanelService;
  public authPanelService: AuthPanelService;
  public showSidebar: boolean;
  private streamSidebar: Subscription;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    @Inject(forwardRef(() => AuthPanelService)) authPanelService,
    private deviceService: DeviceDetectorService
  ) {

    this.adminService = adminService;
    this.authPanelService = authPanelService;
    this.routes = routes;

    this.streamSidebar = this.adminService.sidebarBehaviorSubject.subscribe((show) => {

      this.showSidebar = show;

      if (show) {

        // $('.sidebar .collapse').collapse('hide');
        document.body.classList.add('sidebar-toggled');

      } else {

        document.body.classList.remove('sidebar-toggled');

      }

    });

  }

  ngOnInit() {

    if (!this.deviceService.isDesktop()) {

      this.adminService.toggleSidebar(false);

    }

  }

  ngOnDestroy(): void {

    this.streamSidebar.unsubscribe();

  }

}
