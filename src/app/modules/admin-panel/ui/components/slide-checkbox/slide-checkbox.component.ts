import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-slide-checkbox',
  templateUrl: './slide-checkbox.component.html',
  styleUrls: ['./slide-checkbox.component.scss']
})
export class SlideCheckboxComponent implements OnInit, OnDestroy {

  _checked: boolean;

  @Input()
  checked = false;

  @Output()
  changeChecked = new EventEmitter();

  constructor() { }

  ngOnInit() {

    this._checked = this.checked;

  }

  _changeChecked($event: MouseEvent) {
    console.log($event);
    this._checked = !this._checked;
    this.changeChecked.emit(this._checked);
  }

  ngOnDestroy(): void {
    this.checked = false;
  }
}
