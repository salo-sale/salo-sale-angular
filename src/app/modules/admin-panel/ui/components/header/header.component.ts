import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AuthPanelService} from '../../../../auth-panel/shared/services/auth-panel.service';
import {AdminPanelService} from '../../../shared/services/admin-panel.service';
import {AuthService} from '../../../../../shared/services/auth.service';

@Component({
  selector: 'app-admin-panel-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public adminService: AdminPanelService;

  constructor(
    @Inject(forwardRef(() => AdminPanelService)) adminService,
    public authService: AuthService
  ) {
    this.adminService = adminService;
  }

  ngOnInit() {
  }

  logout() {
    if (confirm('Чи напевно хочеш вийти?') === true) {
      this.authService.logout();
    }
  }
}
