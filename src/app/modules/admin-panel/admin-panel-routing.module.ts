import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './ui/pages/home-page/home-page.component';
import {AdminPanelComponent} from './admin-panel.component';
import {TagsPageComponent} from './ui/pages/tags-page/tags-page.component';
import {AdminPanelCompanyComponent} from './modules/admin-panel-company/admin-panel-company.component';
import {CitiesPageComponent} from './ui/pages/cities-page/cities-page.component';

export const routes: Routes = [{
  path: '',
  component: AdminPanelComponent,
  data: {
    isShow: true,
    parentPath: '/admin/',
    exact: true,
    icon: 'fas fa-fw fa-tachometer-alt',
    title: 'Головна',
    roles: {
      admin: true,
      moderation: true,
      user: false,
      business: false
    }
  },
  children: [{
    path: '',
    component: HomePageComponent,
    data: {
      isShow: false,
      parentPath: '/admin/',
      exact: false,
      icon: 'fas fa-fw fa-tags',
      title: 'Dashboard',
      roles: {
        admin: true,
        moderation: true,
        user: false,
        business: false
      }
    },
  }, {
    path: 'tags',
    component: TagsPageComponent,
    data: {
      isShow: true,
      parentPath: '/admin/',
      exact: false,
      icon: 'fas fa-fw fa-tags',
      title: 'Тегі',
      roles: {
        admin: true,
        moderation: true,
        user: false,
        business: false
      }
    },
  }, {
    path: 'cities',
    component: CitiesPageComponent,
    data: {
      isShow: true,
      parentPath: '/admin/',
      exact: false,
      icon: 'fas fa-city',
      title: 'Локалізація',
      roles: {
        admin: true,
        moderation: true,
        user: false,
        business: false
      }
    },
  }]
}, {
  path: 'company',
  component: AdminPanelComponent,
  loadChildren: './modules/admin-panel-company/admin-panel-company.module#AdminPanelCompanyModule',
  data: {
    isShow: true,
    parentPath: '/admin/',
    exact: false,
    icon: 'fas fa-store',
    title: 'Заклади',
    roles: {
      admin: true,
      moderation: false,
      user: false,
      business: false
    }
  },
}, {
  path: 'point-of-sale',
  component: AdminPanelComponent,
  loadChildren: './modules/admin-panel-point-of-sale/admin-panel-point-of-sale.module#AdminPanelPointOfSaleModule',
  data: {
    isShow: true,
    parentPath: '/admin/',
    exact: false,
    icon: 'fas fa-store-alt',
    title: 'Точки продажу',
    roles: {
      admin: true,
      moderation: false,
      user: false,
      business: false
    }
  },
}, {
  path: 'contract',
  component: AdminPanelComponent,
  loadChildren: './modules/admin-panel-contract/admin-panel-contract.module#AdminPanelContractModule',
  data: {
    isShow: false,
    parentPath: '/admin/',
    exact: false,
    icon: 'fas fa-file',
    title: 'Контракти *',
    roles: {
      admin: true,
      moderation: false,
      user: false,
      business: false
    }
  },
}, {
  path: 'offer',
  component: AdminPanelComponent,
  loadChildren: './modules/admin-panel-offer/admin-panel-offer.module#AdminPanelOfferModule',
  data: {
    isShow: true,
    parentPath: '/admin/',
    exact: false,
    icon: 'fas fa-percent',
    title: 'Пропозиції',
    roles: {
      admin: true,
      moderation: false,
      user: false,
      business: false
    }
  },
}, {
  path: 'user',
  component: AdminPanelComponent,
  loadChildren: './modules/admin-panel-user/admin-panel-user.module#AdminPanelUserModule',
  data: {
    isShow: true,
    parentPath: '/admin/',
    exact: false,
    icon: 'fas fa-users',
    title: 'Користувачі',
    roles: {
      admin: true,
      moderation: false,
      user: false,
      business: false
    }
  },
}, {
  path: 'notification',
  component: AdminPanelComponent,
  loadChildren: './modules/admin-panel-notification/admin-panel-notification.module#AdminPanelNotificationModule',
  data: {
    isShow: true,
    parentPath: '/admin/',
    exact: false,
    icon: 'fas fa-bell',
    title: 'Повідомлення',
    roles: {
      admin: true,
      moderation: false,
      user: false,
      business: false
    }
  },
}, {
  path: 'advertising-banner',
  component: AdminPanelComponent,
  loadChildren: './modules/admin-panel-advertising-banner/admin-panel-advertising-banner.module#AdminPanelAdvertisingBannerModule',
  data: {
    isShow: true,
    parentPath: '/admin/',
    exact: false,
    icon: 'fas fa-ad',
    title: 'Рекламні банери',
    roles: {
      admin: true,
      moderation: false,
      user: false,
      business: false
    }
  },
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class AdminPanelRoutingModule { }
