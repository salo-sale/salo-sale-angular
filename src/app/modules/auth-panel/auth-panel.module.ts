import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthPanelComponent } from './auth-panel.component';
import {AuthPanelRoutingModule} from './auth-panel-routing.module';
import {LoginPageComponent} from './ui/pages/login-page/login-page.component';
import {RegistrationPageComponent} from './ui/pages/registration-page/registration-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import { ConfirmEmailPageComponent } from './ui/pages/confirm-email-page/confirm-email-page.component';
import { ResetPassowrdPageComponent } from './ui/pages/reset-passowrd-page/reset-passowrd-page.component';

@NgModule({
  declarations: [
    AuthPanelComponent,
    LoginPageComponent,
    RegistrationPageComponent,
    ConfirmEmailPageComponent,
    ResetPassowrdPageComponent
  ],
  imports: [
    CommonModule,
    AuthPanelRoutingModule,
    ReactiveFormsModule
  ]
})
export class AuthPanelModule { }
