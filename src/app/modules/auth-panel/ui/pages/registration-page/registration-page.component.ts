import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {RegistrationModel} from '../../../shared/models/registration.model';
import {AuthPanelService} from '../../../shared/services/auth-panel.service';
import {AuthService} from '../../../../../shared/services/auth.service';

@Component({
  selector: 'app-client-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss']
})
export class RegistrationPageComponent implements OnInit {

  public form: RegistrationModel;
  public showLoading: boolean;
  public showGoodStatus: boolean;
  public initialize: boolean;

  constructor(
    public authPanelService: AuthPanelService,
    public authService: AuthService
  ) {

    this.initialize = false;
    this.init();

  }

  private init() {

    if (!this.initialize) {

      this.authPanelService.getAgreements().then((agreements) => {

        console.log(agreements);
        this.form = new RegistrationModel();
        this.form.agreements = this.authPanelService.agreements;
        this.form.init();
        this.initialize = true;


      });

    }

  }

  ngOnInit() {

    this.showLoading = false;
    this.showGoodStatus = false;

  }

  register() {

    this.showLoading = true;

    if (this.form.valid) {

      console.log(this.form);

      this.authService.registration(this.form).then((result) => {

        console.log(result);

        if (result) {

          setTimeout(() => {

            if (this.authService.checkRoleAccess(['SUPER_ADMIN', 'ADMIN'])) {

              this.authService.redirect('/admin/');

            } else {

              this.authService.redirect('/');

            }

          }, 2500);

        } else {

          this.showLoading = false;
          this.showGoodStatus = false;

        }

      });

    }

  }

}
