export interface AgreementInterface {

  id: number;
  url: string;
  required: number;
  agreement: string;

}
