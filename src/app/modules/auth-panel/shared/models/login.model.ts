import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginInterface} from './interfaces/login.interface';
import {EMAIL_PATTERN} from '../../../../shared/tools/patterns.tool';

export class LoginModel implements LoginInterface {

  constructor() {

    this.init();

  }

  get formGroup(): FormGroup {

    return this.formBuilder;

  }

  /**
   *
   * Declaration variables
   *
   */

  public email: string;
  public password: string;

  private formBuilder: FormGroup;

  public showPassword: boolean;
  public showPasswordRepeat: boolean;

  get valid(): boolean {

    if (this.formGroup.valid) {

      this.email = this.formGroup.get('email').value;
      this.password = this.formGroup.get('password').value;

      return true;

    } else {

      return false;

    }

  }

  private init(): void {

    this.email = '';
    this.password = '';

    this.showPassword = false;
    this.showPasswordRepeat = false;

    this.formBuilder = new FormBuilder().group({
      email: [
        this.email,
        [
          Validators.required,
          Validators.pattern(EMAIL_PATTERN)
        ]
      ],
      password: [
        this.password,
        [
          Validators.required,
          Validators.minLength(6),
        ]
      ]
    });

  }

}
