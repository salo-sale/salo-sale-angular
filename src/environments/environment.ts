// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


// const host = '/backend/';
const host = '/';

const hosts = {

  client: host + 'client/api/v1/',
  auth: host + 'auth/',
  admin: host + 'admin/api/v1/',

};

export const environment = {

  production: false,

  host: host,
  hosts: hosts,

  accessForUse: [
    'У всіх торгових точках бренду',
    'У всіх торгівельних точках бренду, вибраного міста у знижці',
    'У вибраних торгівельних точках бренду',
  ],
  accessForUser: [
    'Всім',
    'Підписники'
  ],
  typeOffer: [
    'Всі типи',
    'Пропозиція',
    'Купон',
    'Банер',
    'Подія',
  ],
  typeQrCode: [
    'Виберіть тип',
    'Динамічне посилання',
    'YouTube',
    'WI-FI',
    'Електронна бізнес карточка',
    'SMS',
    'Телефон',
    'MMS',
    'Контакт',
    'Гео локація',
    'Платіжка в біткоінах',
    'Закладка',
    'Електронна пошта',
    'Електроний лист',
    'Подія (Календар)'
  ],

  typeMapQrCode: {
    '': '',
    'DEFAULT': 'Динамічне посилання',
    'YOUTUBE': 'YouTube',
    'WIFI': 'WI-FI',
    'V_CARD': 'Електронна бізнес карточка',
    'SMS': 'SMS',
    'PHONE': 'Телефон',
    'MMS': 'MMS',
    'ME_CARD': 'Контакт',
    'GEO': 'Гео локація',
    'BTC': 'Платіжка в біткоінах',
    'BOOK_MARK': 'Закладка',
    'MAIL_TO': 'Електронна пошта',
    'MAIL_MESSAGE': 'Електроний лист',
    'I_CARD': 'Подія (Календар)'
  },

  defaultDate: {

    languageCode: 'uk',
    localityId: 1,
    offer: {

      price: 50,
      limit: null,
      ikointForTake: 5,
      type: 1,
      accessForUse: 1,
      accessForUser: 1

    },

    objects: {

      offer: 'offer',
      company: 'company',
      event: 'event'

    }

  },

  filter: {

    count: 25,
    page: 1,
    sort: 'DESC',
    orderBy: 'id'

  },

  api: {

    host: host,
    hosts: hosts,
    client_id: '3',
    client_secret: '257afb536932c2c53c4d4d379006c19876598a53',
    language: 'uk',
    device_uuid: null,

    paths: {

      auth: {

        'login': hosts.auth + 'login', // POST
        'registration': hosts.auth + 'registration', // POST
        'confirm-email': hosts.auth + 'confirm-email', // POST
        'logout': hosts.auth + 'logout', // DELETE
        'refresh-token': hosts.auth + 'refresh-token', // GET

        // Reset password
        'forgot-password': hosts.auth + 'forgot-password', // POST
        'check-password-reset-token': hosts.auth + 'check-password-reset-token', // GET
        'reset-password': hosts.auth + 'reset-password', // POST
        'agreements': hosts.auth + 'agreements', // GET

      },

      client: {

        'cities': hosts.client + 'datas/localities',
        'languages': hosts.client + 'datas/languages',
        'listsOfTags': hosts.client + 'datas/tags',
        'user': hosts.client + 'users',
        'user/edit': hosts.client + 'users/edit',
        'invitations': hosts.client + 'users/invitations',

      },

      admin: {

        'dashboards': hosts.admin + 'dashboards', // GET
        // 'migrate-old-db': hosts.admin + 'dashboards/migrate-old-db', // GET

        'offers': hosts.admin + 'offers', // GET, POST
        'offer': hosts.admin + 'offers/', // GET, PUT, POST {id}

        'offer-codes': hosts.admin + 'offer-codes', // GET {offert_id}
        'offer-validity': hosts.admin + 'validity-offers', // GET {offert_id}
        'offer-links': hosts.admin + 'offer-links', // GET {offert_id}

        'notifications': hosts.admin + 'notifications', // GET, POST
        'notification': hosts.admin + 'notifications/', // GET, PUT, POST {id}

        'companies': hosts.admin + 'companies', // GET, POST
        'company': hosts.admin + 'companies/', // GET POST {id}

        'contracts': hosts.admin + 'contracts', // GET, POST
        'contract': hosts.admin + 'contracts/', // GET POST {id}

        'point-of-sale-work-hours': hosts.admin + 'point-of-sales/work-hours', // GET POST PUT

        'advertising-banners': hosts.admin + 'recommendation-banners', // GET, POST
        'advertising-banner': hosts.admin + 'recommendation-banners/', // GET POST {id}

        'points-of-sale': hosts.admin + 'point-of-sales', // GET, POST

        'users': hosts.admin + 'users', // GET
        'user': hosts.admin + 'users/', // GET {id}

        'tags': hosts.admin + 'tags', // GET
        'tag': hosts.admin + 'tags/tag', // POST PUT {languages}
        'tag-to-list-of-tags': hosts.admin + 'tags/tag-to-list-of-tags', // POST DELETE {tag_id} {name}
        'group-tag-to-list-of-tags': hosts.admin + 'tags/group-tag-to-list-of-tags', // POST {tags[{id}]} {name}

        'cities': hosts.admin + 'cities', // GET
        'city': hosts.admin + 'cities/', // GET POST/[id] PUT/[id] DELETE/[id] {languages}

        'qr-codes': hosts.admin + 'qr-codes', // CRUD

      }

    }

  },
  rolesCompany: [
    '',
    'Власник',
    'Менеджер'
  ],
  rolesPointOfSale: [
    '',
    'Власник',
    'Менеджер',
    'Касир'
  ],
  typeContact: [
    '',
    'Телефон',
    'Telegram',
    'Instagram',
    'FaceBook',
    'Веб-портал',
    'E-mail',
    'Viber',
    'vk.com',
    'Slack',
    'LinkedIn',
    'Інше'
  ],
  topics: [
    {
      name: 'client_web',
    },
    {
      name: 'client_ios',
    },
    {
      name: 'client_android',
    },
    {
      name: 'no_authorization',
    },
  ]

};

/*
 * For easier debugging in development mod  e, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.



// Salo-Sale

// firebase: {
//   apiKey: 'AIzaSyC4bEZSDZ0FxX3fLjC8a5Xq55cihMsnWHs',
//   authDomain: 'discountik-ua.firebaseapp.com',
//   databaseURL: 'https://discountik-ua.firebaseio.com',
//   projectId: 'discountik-ua',
//   storageBucket: 'discountik-ua.appspot.com',
//   messagingSenderId: '785113861218',
//   appId: '1:785113861218:web:e36c73da4633af41'
// },
// firebase: {
//   apiKey: 'AIzaSyClRvxbUuprC5Nrt4F8u-l16_o4AqxZjFg',
//   authDomain: 'salo-sale.firebaseapp.com',
//   databaseURL: 'https://salo-sale.firebaseio.com',
//   projectId: 'salo-sale',
//   storageBucket: 'salo-sale.appspot.com',
//   messagingSenderId: '253884189274',
//   appId: '1:253884189274:web:39ad15f77b53b28542e663',
//   measurementId: 'G-7ZNC24ZCED'
// },
// storage: {
//   // host: 'https://firebasestorage.googleapis.com/v0/b/discountik-ua.appspot.com/o/',
//   host: 'https://firebasestorage.googleapis.com/v0/b/salo-sale.appspot.com/o/',
// },
// startRegistrationIkoint: 50
